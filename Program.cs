﻿using System;

namespace BordinBot
{
    class Program
    {
        static void Main(string[] args)
        {
            var bot = new Bot();
            var task = bot.RunAsync();
            var awaiter = task.GetAwaiter();
            awaiter.GetResult();
        }
    }
}
