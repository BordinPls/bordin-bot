﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BordinBot.Util.Date
{
    /// <summary>
    /// A lightweight date class for saving dates without the C# DateTime.
    /// </summary>
    public class Date
    {
        /// <summary>
        /// Number of Month.
        /// </summary>
        public byte Month { get; set; }

        /// <summary>
        /// Number of Day.
        /// </summary>
        public byte Day { get; set; }

        /// <summary>
        /// Year of occurrence.
        /// </summary>
        public ushort Year { get; set; }

        public Date()
        {
            Month = 0;
            Day = 0;
            Year = 0;
        }

        public bool IsBlank()
        {
            return (Month == 0) && (Year == 0) && (Day == 0);
        }

        public override bool Equals(object obj)
        {
            //Check for null and compare run-time types.
            if ((obj == null) || !this.GetType().Equals(obj.GetType()))
            {
                return false;
            }
            else
            {
                Date date = (Date)obj;
                return (Month == date.Month) && (Year == date.Year) && (Day == date.Day);
            }
        }

        public override int GetHashCode()
        {
            return Month.GetHashCode() + Day.GetHashCode() + Year.GetHashCode();
        }

        public override string ToString()
        {
            return string.Format("{0:D2}/{1:D2}/{2}", Month, Day, Year);
        }

    }
}
