﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using DSharpPlus.Entities;

namespace BordinBot.Util.Scheduler
{
    public class ScheduledCommand
    {
        #region Constructor
        /// <summary>
        /// Constructs a ScheduledCommand with the given parameters.
        /// </summary>
        /// <param name="task">Function to execute.</param>
        /// <param name="channel">DiscordChannel on which to send messages (if the task needs them)</param>
        /// <param name="executionTime">Time at which a task will be executed. For repeating tasks, will dictate
        /// the timne of day/month they will repeat at.</param>
        /// <param name="frequency">Frequency at which to execute tasks.</param>
        /// <exception cref="ArgumentNullException"></exception>
        public ScheduledCommand(Action<DiscordChannel> task, DiscordChannel channel, DateTime executionTime, EFrequency frequency)
        {
            // Null checks
            if (task == null)
                throw new ArgumentNullException(nameof(task));
            if (channel == null)
                throw new ArgumentNullException(nameof(channel));

            mTaskToExecute = task;
            mChannel = channel;
            mExecutionTime = executionTime;
            mFrequency = frequency;

            mTimer.Elapsed += OnTimerElapsed;
            mTimer.AutoReset = frequency != EFrequency.Once ? true : false; // Don't reset if timing is Once

            CalculateNextExecutionTime();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Timer that ticks down to execute a this scheduled task.
        /// </summary>
        protected Timer mTimer = new();

        /// <summary>
        /// Function to execute when timer elapses.
        /// </summary>
        protected Action<DiscordChannel> mTaskToExecute;

        /// <summary>
        /// Discord channel that the task can utilize if it needs to send messages.
        /// </summary>
        protected DiscordChannel mChannel;

        /// <summary>
        /// Gets what time/day the task should execute at this task's frequency.
        /// </summary>
        public DateTime ExecutionTime
        {
            get { return mExecutionTime; }
        }

        /// <summary>
        /// Defines what time/day the task should execute at this task's frequency.
        /// </summary>
        protected DateTime mExecutionTime;

        /// <summary>
        /// Gets what time/day the task should execute at this task's frequency.
        /// </summary>
        public DateTime NextExecutionTime
        {
            get { return mNextExecutionTime; }
        }

        /// <summary>
        /// Time at which this task will next be executed.
        /// </summary>
        protected DateTime mNextExecutionTime;

        /// <summary>
        /// Gets at which this task will next be executed.
        /// </summary>
        public EFrequency Frequency
        {
            get { return mFrequency; }
        }

        /// <summary>
        /// Frequency at which the task will execute.
        /// </summary>
        protected EFrequency mFrequency;

        #endregion

        #region Methods

        /// <summary>
        /// Starts the timer for this task. Will determine and set the timer
        /// to count down to the next execution time. Will also stop the timer
        /// if it's already running.
        /// </summary>
        public void StartTimer()
        {
            // If it's already running, stop it
            mTimer.Stop();

            // Determine next execution time
            CalculateNextExecutionTime();

            // Set timer interval with seconds until the determined time
            double executionTimeInMilliseconds = TimeUntilExecution().TotalMilliseconds;
            mTimer.Interval = executionTimeInMilliseconds > Int32.MaxValue ? Int32.MaxValue : executionTimeInMilliseconds; // Cap to max int32 if needed
            mTimer.Start();
        }

        /// <summary>
        /// Invokes the task stored on this object. If it is not within one minute of the scheduled task time,
        /// the timer will restart and continue ticking until the next execution time set on this object. If the
        /// task truly did execute on time, and the
        /// task is supposed to repeat, then the timer will restart.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        public void OnTimerElapsed(object source, System.Timers.ElapsedEventArgs e)
        {
            // if time is within 1 minute
            if (Math.Abs(TimeUntilExecution().TotalMinutes) <= 1)
            {
                mTaskToExecute.Invoke(mChannel);
                CalculateNextExecutionTime();

                // If this task is supposed to repeat, restart the timer
                if (FrequencyHelpers.IsRepeatingFrequency(mFrequency))
                {
                    StartTimer();
                }
            }
            // Else, the timer elapsed but it's not actually at the appropriate
            // execution time for this ScheduledTask (probably because the original
            // time exceeded the max interval for a timer)
            else
            {
                // Restart timer to continue ticking until appropriate execution time
                StartTimer();
            }
        }

        /// <summary>
        /// Returns the amount of time from the current moment until the next
        /// time this task ought to execute. This is not reflective of the actual time remaining
        /// on the internal timer, but rather the real world time until this ought to execute.
        /// </summary>
        /// <returns></returns>
        public TimeSpan TimeUntilExecution()
        {
            return mNextExecutionTime - DateTime.Now;
        }

        /// <summary>
        /// Updates the next execution time for this task based on its current frequency and execution
        /// time pattern.
        /// </summary>
        /// <exception cref="InvalidExecutionTimeException"></exception>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        protected void CalculateNextExecutionTime()
        {
            var now = DateTime.Now;
            switch (mFrequency)
            {
                case EFrequency.Once:
                    // Check if time has already passed for the task
                    if (ExecutionTime < DateTime.Now)
                        throw new InvalidExecutionTimeException("DateTime has already passed for task with 'once' frequency");

                    mNextExecutionTime = ExecutionTime;
                    break;
                case EFrequency.Hourly:
                    mNextExecutionTime = FrequencyHelpers.GetNextTime_Hourly(mExecutionTime);
                    break;
                case EFrequency.Daily:
                    mNextExecutionTime = FrequencyHelpers.GetNextTime_Daily(mExecutionTime);
                    break;
                case EFrequency.Weekly:
                    mNextExecutionTime = FrequencyHelpers.GetNextTime_Weekly(mExecutionTime);
                    break;
                case EFrequency.Monthly:
                    mNextExecutionTime = FrequencyHelpers.GetNextTime_Monthly(mExecutionTime);
                    break;
                case EFrequency.Yearly:
                    mNextExecutionTime = FrequencyHelpers.GetNextTime_Monthly(mExecutionTime);
                    break;
                default:
                    throw new ArgumentOutOfRangeException("Given value is not a valid EFrequency");
            }
        }

        #endregion

        /// <summary>
        /// Thrown when a task is scheduled for a time it cannot achieve, e.g. a
        /// task with a "once" frequency set for a date that has already past
        /// </summary>
        [Serializable]
        public class InvalidExecutionTimeException : Exception
        {
            public InvalidExecutionTimeException() { }
            public InvalidExecutionTimeException(string message) : base(message) { }
            public InvalidExecutionTimeException(string message, Exception inner) : base(message, inner) { }
            protected InvalidExecutionTimeException(
              System.Runtime.Serialization.SerializationInfo info,
              System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
        }
    }
}
