﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BordinBot.Util.Scheduler
{
    /// <summary>
    /// Type of frequency for a scheduled task.
    /// </summary>
    public enum EFrequency
    {
        Once,
        Hourly,
        Daily,
        Weekly,
        Monthly,
        Yearly
    }

    /// <summary>
    /// Provides functions useful for determining the 
    /// </summary>
    public static class FrequencyHelpers
    {
        /// <summary>
        /// Gets the next time that a task would be executed given an hourly frequency relative
        /// to the current system time.
        /// </summary>
        /// <param name="pattern">Time pattern for figuring out next execution time. Since this is hourly,
        /// only the minutes of this pattern will be significant.</param>
        public static DateTime GetNextTime_Hourly(DateTime pattern)
        {
            // Clone the current time but use the pattern's minutes
            DateTime currentTime = DateTime.Now;
            DateTime targetDatetime = new DateTime(currentTime.Year, currentTime.Month, currentTime.Day, currentTime.Hour, pattern.Minute, pattern.Second);

            // If the current time is past the target, then add 1 hour to the target, then return
            // e.g. if it's 8:45 and target time is X:35 (now set to 8:35), then 9:35 would be the next triggering time
            if (currentTime > targetDatetime)
            {
                return targetDatetime.AddHours(1);
            }
            // Else, the pattern time is still upcoming
            // e.g. current time is 8:45 and target is X:55, then 8:55 would be next triggering time
            else
            {
                return targetDatetime;
            }
        }

        /// <summary>
        /// Gets the next time that a task would be executed given an daily frequency relative
        /// to the current system time.
        /// </summary>
        /// <param name="pattern">Time pattern for figuring out next execution time. Since this is hourly,
        /// only the minutes of this pattern will be significant.</param>
        public static DateTime GetNextTime_Daily(DateTime pattern)
        {
            // Clone the current time but use the pattern's significant times
            DateTime currentTime = DateTime.Now;
            DateTime targetDatetime = new DateTime(currentTime.Year, currentTime.Month, currentTime.Day, pattern.Hour, pattern.Minute, pattern.Second);

            // If the current time is past the target, then add 1 day to the target, then return
            if (currentTime > targetDatetime)
            {
                return targetDatetime.AddDays(1);
            }
            // Else, the pattern time is still upcoming
            // e.g. current time is 8:45 and target is X:55, then 8:55 would be next triggering time
            else
            {
                return targetDatetime;
            }
        }

        /// <summary>
        /// Gets the next time that a task would be executed given an weekly frequency relative
        /// to the current system time.
        /// </summary>
        /// <param name="pattern">Time pattern for figuring out next execution time. Since this is hourly,
        /// only the minutes of this pattern will be significant.</param>
        public static DateTime GetNextTime_Weekly(DateTime pattern)
        {
            // Clone the current time but use the pattern's significant times
            DateTime currentTime = DateTime.Now;
            DateTime targetDatetime = new DateTime(currentTime.Year, currentTime.Month, currentTime.Day, pattern.Hour, pattern.Minute, pattern.Second);

            // If it is the same day as today...
            if (currentTime.DayOfWeek == pattern.DayOfWeek)
            {
                // if it hasn't passed yet, return the target time, since it's happening later today 
                if (currentTime < targetDatetime)
                {
                    return targetDatetime;
                }
            }

            // If it's not the same day of the week, or it's today but already passed...
            // Shift today's date to the same day of the week as the pattern
            int daysUntil = 7 - (currentTime.DayOfWeek - pattern.DayOfWeek);
            targetDatetime = targetDatetime.AddDays(daysUntil);

            return targetDatetime;
        }

        /// <summary>
        /// Gets the next time that a task would be executed given an monthly frequency relative
        /// to the current system time.
        /// </summary>
        /// <param name="pattern">Time pattern for figuring out next execution time. Since this is hourly,
        /// only the minutes of this pattern will be significant.</param>
        public static DateTime GetNextTime_Monthly(DateTime pattern)
        {
            // Clone the current time but use the pattern's significant times
            DateTime currentTime = DateTime.Now;
            DateTime targetDatetime = new DateTime(currentTime.Year, currentTime.Month, pattern.Day, pattern.Hour, pattern.Minute, pattern.Second);

            // If the current time is past the target, then add 1 month
            if (currentTime > targetDatetime)
            {
                return targetDatetime.AddMonths(1);
            }
            // Else, the pattern time is still upcoming
            else
            {
                return targetDatetime;
            }
        }

        /// <summary>
        /// Gets the next time that a task would be executed given an yearly frequency relative
        /// to the current system time.
        /// </summary>
        /// <param name="pattern">Time pattern for figuring out next execution time. Since this is hourly,
        /// only the minutes of this pattern will be significant.</param>
        public static DateTime GetNextTime_Yearly(DateTime pattern)
        {
            // Clone the current time but use the pattern's significant times
            DateTime currentTime = DateTime.Now;
            DateTime targetDatetime = new DateTime(currentTime.Year, pattern.Month, pattern.Day, pattern.Hour, pattern.Minute, pattern.Second);

            // If the current time is past the target, then add 1 year
            if (currentTime > targetDatetime)
            {
                return targetDatetime.AddYears(1);
            }
            // Else, the pattern time is still upcoming
            else
            {
                return targetDatetime;
            }
        }

        /// <summary>
        /// Returns whether the given frequency is a repeating one.
        /// </summary>
        /// <param name="inFrequency">Frequency to evaluate.</param>
        /// <returns>True if the frequency is anything other than "Once". False otherwise</returns>
        public static bool IsRepeatingFrequency(EFrequency inFrequency)
        {
            return !(inFrequency == EFrequency.Once);
        }
    }

}
