﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using DSharpPlus.Entities;

namespace BordinBot.Util.Scheduler
{
    /// <summary>
    /// Maintains a list of scheduled tasks. Should have a more complex scheduling algorithm
    /// but I'm too lazy to create something so complex for what should be a minute number of tasks. Keeping
    /// the design like this so I can resolve that later.
    /// </summary>
    public class Scheduler
    {
        /// <summary>
        /// Internal static reference to class for Singleton pattern.
        /// </summary>
        private static Scheduler _instance;

        /// <summary>
        /// List of all currently running tasks.
        /// </summary>
        private List<ScheduledCommand> mTasks = new List<ScheduledCommand>();

        /// <summary>
        /// Private constructor.
        /// </summary>
        private Scheduler() { }

        /// <summary>
        /// Returns the singleton instance of this class.
        /// </summary>
        public static Scheduler Instance => _instance ?? (_instance = new Scheduler());

        /// <summary>
        /// DiscordChannel to provide to tasks if they need context to send a message
        /// without having the usual context via user command triggering.
        /// </summary>
        private DiscordChannel DefaultChannel;

        /// <summary>
        /// Sets the Discord channel that will be passed to schedulede tasks
        /// so they can send messages without a context.
        /// </summary>
        /// <param name="channel">DiscordChannel for tasks to use.</param>
        /// <exception cref="ArgumentNullException"></exception>
        public void SetDefaultDiscordChannel(DiscordChannel channel)
        {
            // Null check
            if (channel == null)
                throw new ArgumentNullException(nameof(channel));

            DefaultChannel = channel;
        }

        /// <summary>
        /// Creates a scheduled task given the function and time parameters.
        /// </summary>
        /// <param name="task">Task to execute periodically.</param>
        /// <param name="executionTime">Time at which a task will be executed. For repeating tasks, will dictate
        /// the timne of day/month they will repeat at.</param>
        /// <param name="frequency">Frequency at which to execute tasks.</param>
        public void ScheduleTask(Action<DiscordChannel> task, DateTime executionTime, EFrequency frequency)
        {
            ScheduledCommand scheduledCommand = new ScheduledCommand(task, DefaultChannel, executionTime, frequency);
            mTasks.Add(scheduledCommand);
            scheduledCommand.StartTimer();
        }
    }
}
