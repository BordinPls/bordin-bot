﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace BordinBot.Util.Logging
{
    /// <summary>
    /// A static class that holds a reference to the DSharpPlus logger so messages can be logged
    /// using a common, prettier format.
    /// </summary>
    /// <exception cref="ArgumentNullException">Thrown when a parameter is null.</exception>
    internal class Logger
    {
        public static void WriteLine(in string message, params object[] args)
        {
            if (message == null)
                throw new ArgumentNullException(nameof(message));

            // Make sure there's a logger first
            if (mLogger != null)
            {
                mLogger.Log(LogLevel.Debug, message, args);
            }
            // If there's no pretty logger available, dump the message to console
            else
            {
                Console.WriteLine(message);
            }
        }

        public static void WriteLine( in string message, LogLevel level, params object[] args)
        {
            if(message == null)
                throw new ArgumentNullException(nameof(message));

            // Make sure there's a logger first
            if(mLogger != null)
            {
                mLogger.Log(level, message, args);
            }
            // If there's no pretty logger available, dump the message to console
            else
            {
                Console.WriteLine(message);
            }
        }

        public static void SetLogger(ILogger inLogger)
        {
            if(inLogger == null)
                throw new ArgumentNullException(nameof(inLogger));

            mLogger = inLogger;
        }

        private static ILogger mLogger;
    }
}
