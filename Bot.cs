﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using BordinBot.Commands;
using BordinBot.Util.Logging;
using BordinBot.Util.Scheduler;
using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.Entities;
using DSharpPlus.EventArgs;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace BordinBot
{
    public class Bot
    {
        private const string CONFIG_FILE_NAME = "config.json";

        public DiscordClient mClient { get; private set; }
        public CommandsNextExtension mCommands { get; private set; }
        /// <summary>
        /// Primary channel out which the bot will send updates or messages.
        /// </summary>
        public DiscordChannel mDefaultMessageChannel { get; private set; }
        /// <summary>
        /// Configuration info set by user for the bot. Read from file given by CONFIG_FILE_NAME.
        /// </summary>
        private BotConfig mConfiguration;

        ~Bot()
        {
            mClient.Logger.LogInformation("Shutting down...");
        }

        public async Task RunAsync()
        {
            Console.WriteLine("Starting bot...");

            // Attempt to load config file first
            Console.WriteLine("Reading configuration file...");
            try
            {
                mConfiguration = ReadConfigFile();
                BotTools.Configuration = mConfiguration;
            }
            catch (Exception e)
            {
                Console.WriteLine(string.Format("Failed to open JSON File! : {0}", e.Message));
                ExitProgram();
            }

            // Create Discord Configuration from settings
            var discordConfig = new DiscordConfiguration
            {
                Token = mConfiguration.Token,
                TokenType = TokenType.Bot,
                AutoReconnect = true,
                MinimumLogLevel = Microsoft.Extensions.Logging.LogLevel.Debug,
                Intents = DiscordIntents.AllUnprivileged | DiscordIntents.MessageContents
            };

            // Create discord client
            mClient = new DiscordClient(discordConfig);
            BotTools.Client = mClient;
            mClient.Ready += OnClientReady;


            // Set logger
            Logger.SetLogger(mClient.Logger);

            // Get default message channel
            try
            {
                mDefaultMessageChannel = mClient.GetChannelAsync(mConfiguration.DefaultChannelID).Result;
            }
            // If failed, exit
            catch (Exception e)
            {
                mClient.Logger.LogError(e, "Failed to set default message channel! Attempted to use ID {0} but no channel was found.", mConfiguration.DefaultChannelID);
                ExitProgram(-1);
            }
            Scheduler.Instance.SetDefaultDiscordChannel(mDefaultMessageChannel);

            // Create configuration for CommandsNext
            var commandsConfig = new CommandsNextConfiguration
            {
                StringPrefixes = new string[] { mConfiguration.Prefix },
                EnableMentionPrefix = true,
                EnableDms = false,
                CaseSensitive = false,
                DmHelp = false,
                EnableDefaultHelp = true,
                QuotationMarks = new char[] { '"', '“', '”' }

            };

            // Register command modules
            mCommands = mClient.UseCommandsNext(commandsConfig);
            mCommands.CommandErrored += OnCommandError;
            RegisterCommandModules();


            // Attempt to connect to server
            mClient.Logger.LogInformation("Attempting async connect...");
            await mClient.ConnectAsync();

            await Task.Delay(-1);

        }

        /// <summary>
        /// Registers all BordinBot command modules for DSharpPlus.
        /// </summary>
        private void RegisterCommandModules()
        {
            mCommands.RegisterCommands<BasicCommands>();
            mCommands.RegisterCommands<FunCommands>();
            mCommands.RegisterCommands<QuoteCommands>();
            mCommands.RegisterCommands<CounterCommands>();
            mCommands.RegisterCommands<BirthdayCommands>();
        }

        /// <summary>
        /// Called when the Discord client has connected. Signals the bot is online and ready.
        /// </summary>
        /// <param name="client"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        private Task OnClientReady(DiscordClient client, ReadyEventArgs e)
        {
            mClient.Logger.LogInformation("Bot is online!");
            if (mConfiguration.SendAwakeMessage)
                SendMessageOnDefaultChannel(mConfiguration.AwakeMessage);

            return Task.CompletedTask;
        }

        private Task OnCommandError(CommandsNextExtension cmds, CommandErrorEventArgs args)
        {
            if (args.Command == null)
            {
                args.Context.Message.RespondAsync($"No command with that name was found! Check your spelling.");
            }
            else
            {
                mClient.Logger.LogError("[{0}] - {1}: {2}", args.Command.Module.ModuleType.Name, args.Command.Name, args.Exception.Message);
                args.Context.RespondAsync($"[ERROR - {args.Command.Module.ModuleType.Name} : {args.Command.Name}] {args.Exception.Message}");
            }
            return Task.CompletedTask;
        }

        /// <summary>
        /// Sends the given message out to the default Discord channel, if one was configured.
        /// </summary>
        /// <param name="message">Message to send.</param>
        /// <returns></returns>
        public void SendMessageOnDefaultChannel(string message)
        {
            try
            {
                // If a default message channel is set...
                if (mDefaultMessageChannel != null)
                    mClient.SendMessageAsync(mDefaultMessageChannel, message);
                else
                    mClient.Logger.LogWarning("Attempted to write to default channel, but none is currently set!");
            }
            catch (Exception e)
            {
                mClient.Logger.LogError(e, "Could not send message on default channel!");
            }
        }

        /// <summary>
        /// Attempts to read BotConfig data from the config.json file in the same directory.
        /// </summary>
        /// <returns>The BotConfig data from the file, or null if unsuccessful.</returns>
        private BotConfig ReadConfigFile()
        {
            var json = string.Empty;

            // Open file and pull out JSON string
            using (var file = File.OpenRead(CONFIG_FILE_NAME))
            using (var streamReader = new StreamReader(file, new UTF8Encoding(false)))
                json = streamReader.ReadToEnd();

            // Try to load JSON file
            try
            {
                var config = JsonConvert.DeserializeObject<BotConfig>(json);
                return config;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Exits the program, using the given exit code.
        /// </summary>
        /// <param name="exitCode">Exit code for the program. Defaults to 0.</param>
        private void ExitProgram(int exitCode = 0)
        {
            System.Environment.Exit(exitCode);
        }
    }
}
