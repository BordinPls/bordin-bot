﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using DSharpPlus;

namespace BordinBot
{
    /// <summary>
    /// Collection of different static tools and components for the bot that other facets of BordinBot may make use of. Intended
    /// to provide a nice container for any kind of tools that may require state, like HttpClients.
    /// </summary>
    public static class BotTools
    {
        public static DiscordClient Client { get; set; }

        /// <summary>
        /// Returns the loaded configuration for this bot. Set by the Bot at initialization.
        /// </summary>
        public static BotConfig Configuration { get; set; }

        /// <summary>
        /// Used to make HTTP requests for web pages.
        /// </summary>
        public static HttpClient HttpRequestClient
        {
            get { return mHttpClient; }
        }

        /// <summary>
        /// Used to make HTTP requests for webpages. Singular instance since it's not supposed to be instantiated (see MS Docs)
        /// </summary>
        private static readonly HttpClient mHttpClient = new HttpClient();
    }
}
