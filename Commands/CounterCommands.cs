﻿using System;
using System.Text;
using System.Threading.Tasks;
using BordinBot.Commands.Counters;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;


namespace BordinBot.Commands
{
    class CounterCommands : BaseCommandModule
    {
        private CounterManager mCounterManager = new CounterManager();

        [Command("counters")]
        [Description("Displays all of the currently loaded counters.")]
        public async Task GetCounters(CommandContext context)
        {
            var counters = mCounterManager.GetAllCounters();

            // If none are loaded, throw
            if (counters.Count < 1)
            {
                throw new Exception("No counters exist!");
            }

            // Start the embed
            DiscordEmbedBuilder embedBuilder = new();
            embedBuilder.Title = "Counters";

            // Fill embed content with all names
            StringBuilder strBuilder = new StringBuilder();
            foreach (var counter in counters)
            {
                strBuilder.AppendLine($"`{counter.Item1}`: {counter.Item2}");
            }
            embedBuilder.Description = strBuilder.ToString();

            await context.RespondAsync(embedBuilder.Build()).ConfigureAwait(false);
        }

        [Command("count")]
        [Description("Displays the current number for a counter with a given name.")]
        public async Task Count(CommandContext context,
            [Description("Name of the counter to check the number of. Case insensitive.")] string counterName)
        {
            var counterStatus = mCounterManager.GetCounterStatus(counterName, context);

            DiscordEmbedBuilder builder = new DiscordEmbedBuilder();
            builder.Title = counterStatus.Name;
            builder.Description = $"*{counterStatus.Description}*:\n\n{counterStatus.Value}";

            await context.RespondAsync(builder.Build()).ConfigureAwait(false);
        }

        [Command("inc")]
        [Description("Adds 1 to the number for a counter.")]
        public async Task Increment(CommandContext context,
            [Description("Name of the counter to increase. Case insensitive.")] string counterName)
        {
            mCounterManager.IncrementCounter(counterName);
            await context.RespondAsync($"Incremented '{counterName}'.").ConfigureAwait(false);
        }

        [Command("inc")]
        [Description("Adds 1 to the number for a specific person on a counter .")]
        public async Task Increment(CommandContext context,
            [Description("Name of the counter to increase. Case insensitive.")] string counterName,
            [Description("Discord @ of the person associated with the counter.")] DiscordUser person)
        {
            mCounterManager.IncrementCounter(counterName, person.ToString());
            await context.RespondAsync($"Incremented '{counterName}' for {person}.").ConfigureAwait(false);
        }

        [Command("dec")]
        [Description("Subtracts 1 from the number for a counter. Case insensitive.")]
        public async Task Decrement(CommandContext context,
            [Description("Name of the counter to decrease.")] string counterName)
        {
            mCounterManager.DecrementCounter(counterName);
            await context.RespondAsync($"Decreased '{counterName}'.").ConfigureAwait(false);
        }
        [Command("dec")]
        [Description("Subtracts 1 from the number for a specific person for a counter. Case insensitive.")]
        public async Task Decrement(CommandContext context,
            [Description("Name of the counter to decrease.")] string counterName,
            [Description("Discord @ of the person associated with the counter.")] DiscordUser person)
        {
            mCounterManager.DecrementCounter(counterName, person.ToString());
            await context.RespondAsync($"Decreased '{counterName}' for {person}.").ConfigureAwait(false);
        }

        [Command("setcounter")]
        [Description("Sets the number for a counter to a value. Cannot be negative or too large. Counter must also have only one value.")]
        public async Task SetCounter(CommandContext context,
            [Description("Name of the counter to set.")] string counterName,
            [Description("Number to set the counter to.")] uint number)
        {
            mCounterManager.SetCounterValue(counterName, number);
            await context.RespondAsync($"Set '{counterName}'. Count is now: {number}").ConfigureAwait(false);
        }

        [Command("setcounter")]
        [Description("Sets the number for a counter to a value for a user. Cannot be negative or too large.")]
        public async Task SetCounter(CommandContext context,
            [Description("Name of the counter to set.")] string counterName,
            [Description("Discord @ of the person associated with the counter.")] DiscordUser person,
            [Description("Number to set the counter to.")] uint number)
        {
            mCounterManager.SetCounterValue(counterName, number, person.ToString());
            await context.RespondAsync($"Set '{counterName}' for '{person.Username}' to {number}").ConfigureAwait(false);
        }

        [Command("createcounter")]
        [Description("Creates a new counter that has to be manually incremented.")]
        public async Task AddCounter(CommandContext context,
            [Description("Name of the counter to set.")] string counterName,
            [Description("Description for this counter, e.g. 'Number of times x has happened'")] string counterDescription)
        {
            mCounterManager.AddCounter(counterName, counterDescription);
            await context.RespondAsync($"Created Counter '{counterName}'.").ConfigureAwait(false);
        }

        [Command("createcounter")]
        [Description("Creates a new counter that tracks how many times a phrase has been said across channels.")]
        public async Task AddCounter(CommandContext context,
            [Description("Name of the counter to set.")] string counterName,
            [Description("Description for this counter, e.g. 'Number of times x has happened'")] string counterDescription,
            [Description("The phrase to search for.")] string phrase)
        {
            mCounterManager.AddCounter(counterName, counterDescription, phrase);
            await context.RespondAsync($"Created Counter '{counterName}'.").ConfigureAwait(false);
        }


        [Command("addcountervalue")]
        [Description("Adds a new value associated with a Discord User to a counter.")]
        public async Task AddCounterValue(CommandContext context,
            [Description("Name of the counter to set.")] string counterName,
            [Description("'normal' for a simple number that ticks up. 'time' to track how much time has passed since this count was altered.")] string type,
            [Description("@ of user that this counter is associated with. If using a scanning counter, this must be the @ of the person to scan messages from. Leave as \"\" for a tracking counter to scan messages from anybody for a phrase.")] DiscordUser user,
            [Description("Optional. Value to start the counter at. For a 'time' value, 0 means 'right now'.")] uint startingValue = 0)
        {
            // For some reason, converting a Discord @ results in an oddly converted UTF-8 string. So decode it first, just in case
            mCounterManager.AddCounterValue(counterName, type, user.Mention);
            await context.RespondAsync($"Added a new value to '{counterName}'.").ConfigureAwait(false);
        }

        [Command("addcountervalue")]
        [Description("Adds a new value associated with name or thing to a counter.")]
        public async Task AddCounterValue(CommandContext context,
            [Description("Name of the counter to set.")] string counterName,
            [Description("'normal' for a simple number that ticks up. 'time' to track how much time has passed since this count was altered.")] string type,
            [Description("Optional. Name of person or thing that this value is associated with. Leave as \"\" for a tracking counter to scan messages from anybody for a phrase.")] string user = null,
            [Description("Optional. Value to start the counter at. For a 'time' value, 0 means 'right now'.")] uint startingValue = 0)
        {
            mCounterManager.AddCounterValue(counterName, type, user == null ? "" : user, startingValue);
            await context.RespondAsync($"Added a new value to '{counterName}'.").ConfigureAwait(false);
        }

        [Command("deletecounter")]
        [Description("Deletes a Counter.")]
        public async Task DeleteCounter(CommandContext context,
            [Description("Name of the counter to delete.")] string counterName)
        {
            mCounterManager.RemoveCounter(counterName);
            await context.RespondAsync($"Deleted '{counterName}'.").ConfigureAwait(false);
        }
    }
}
