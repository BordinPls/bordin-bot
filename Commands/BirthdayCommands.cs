﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BordinBot.Commands.Birthdays;
using BordinBot.Util.Scheduler;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;

namespace BordinBot.Commands
{
    public class BirthdayCommands : BaseCommandModule
    {
        public BirthdayCommands()
        {
            AddScheduledTasks();
        }

        /// <summary>
        /// Manager for keeping track of all birthday data.
        /// </summary>
        private BirthdayManager manager = new BirthdayManager();

        [Command("addbirthday")]
        [Description("Adds a new birthday to the list of birthdays.")]
        public async Task AddBirthday(CommandContext context,
            [Description("Use the '@' for the user.")] DiscordUser user,
            [Description("Format the date \"MM/DD/YYYY\".")] string date)
        {
            manager.AddBirthday(user.Mention, date);
            await context.RespondAsync($"Added the new birthday for {user.Username}!").ConfigureAwait(false);
        }

        [Command("removebirthday")]
        [Description("Removes a birthday from the list of birthdays.")]
        public async Task RemoveBirthday(CommandContext context, DiscordUser user)
        {
            manager.RemoveBirthday(user.Mention);
            await context.RespondAsync($"Removed the birthday for {user.Username}!").ConfigureAwait(false);
        }

        [Command("birthdays")]
        [Description("Lists upcoming birthdays (if any) for this month and next month.")]
        public async Task GetBirthdays(CommandContext context)
        {
            var birthdays = manager.GetBirthdays_Upcoming();
            var message = FormatBirthdays(birthdays);

            await context.RespondAsync(message).ConfigureAwait(false);
        }

        [Command("birthdays")]
        [Description("Lists upcoming birthdays for a given month.")]
        public async Task GetBirthdays(CommandContext context,
            [Description("Number of the month to get the birthdays for. Use 0 for all months.")] int month)
        {
            var birthdays = manager.GetBirthdays_ByMonth(month);
            var message = FormatBirthdays(birthdays);

            await context.RespondAsync(message).ConfigureAwait(false);
        }

        [Command("allbirthdays")]
        [Description("Lists all birthdays for the server.")]
        public async Task GetAllBirthdays(CommandContext context)
        {
            var birthdays = manager.GetBirthdays_All();
            var message = FormatBirthdays(birthdays);

            await context.RespondAsync(message).ConfigureAwait(false);
        }

        private void Task_AnnounceBirthdays_Today(DiscordChannel channel)
        {
            // Get today's birthday(s)
            var birthdays = manager.GetBirthdays_Today();
            if (birthdays.Count > 0)
            {
                var birthday = birthdays.First();
                DiscordMessageBuilder builder = new DiscordMessageBuilder().WithAllowedMention(EveryoneMention.All).
                    WithContent($"@everyone! {birthday.UserString} turns {birthday.Age()} today! Everyone wish them a happy birthday OR ELSE");
                channel.SendMessageAsync(builder);
            }
        }

        private void Task_AnnounceBirthdays_Soon(DiscordChannel channel)
        {
            // Get upcoming birthday(s)
            var birthdays = manager.GetBirthdays_Upcoming();
            if (birthdays.Count > 0)
            {
                var message = FormatBirthdays(birthdays);
                channel.SendMessageAsync(message);
            }
        }

        /// <summary>
        /// Registers birthday-related scheduled tasks to the Scheduler.
        /// </summary>
        private void AddScheduledTasks()
        {
            Scheduler.Instance.ScheduleTask(Task_AnnounceBirthdays_Today, DateTime.Parse("10:30 AM"), EFrequency.Daily);
            Scheduler.Instance.ScheduleTask(Task_AnnounceBirthdays_Soon, DateTime.Parse("01/01/2020 10:00 AM"), EFrequency.Monthly);
        }

        /// <summary>
        /// Returns a DiscordEmbed message with a neatly formatted list of the birthdays for the server
        /// </summary>
        /// <param name="birthdays">List of birthdays to format</param>
        /// <returns></returns>
        private DiscordEmbed FormatBirthdays(List<Birthday> birthdays)
        {
            DiscordEmbedBuilder embedBuilder = new DiscordEmbedBuilder();

            // Set title
            embedBuilder.Title = "Birthdays";

            // If there are any birthdays to list...
            if (birthdays.Count > 0)
            {
                // Get all of the birthdays into buckets based on birth month
                Dictionary<int, List<Birthday>> birthdaysByMonth = new Dictionary<int, List<Birthday>>();

                // For each birthday, we're going to build a field with
                // the month name and each person that has a bday in it
                foreach (var birthday in birthdays)
                {
                    // If there's no entry for the month, make one
                    if (!birthdaysByMonth.ContainsKey(birthday.Birthdate.Month))
                    {
                        birthdaysByMonth.Add(birthday.Birthdate.Month, new List<Birthday>());
                    }

                    birthdaysByMonth[birthday.Birthdate.Month].Add(birthday);
                }

                // Once done, for each month, add a field with a list of birthdays for each
                var birthdaysByMonthSorted = birthdaysByMonth.OrderBy(pair => pair.Key);
                foreach (var birthdayPair in birthdaysByMonthSorted)
                {
                    // Add a line for each name
                    var sortedBirthdays = birthdayPair.Value.OrderBy(birthday => birthday.Birthdate.Day);
                    StringBuilder descriptionBuilder = new StringBuilder();
                    foreach (var birthday in sortedBirthdays)
                    {
                        descriptionBuilder.AppendLine($"{birthday.Birthdate.ToString("M/d")}:    {birthday.UserString} turns {birthday.ComingAge()}");
                    }

                    // Finish the embed field
                    embedBuilder.AddField(birthdayPair.Value.First().Birthdate.ToString("MMMM"), descriptionBuilder.ToString());
                }
            }
            // Else, just not there are none upcoming
            else
            {
                embedBuilder.Color = DiscordColor.Red;
                embedBuilder.Description = "There are no upcoming birthdays.";
            }

            return embedBuilder.Build();
        }
    }
}