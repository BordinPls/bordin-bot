﻿namespace BordinBot.Commands.Counters
{
    internal class ScannedPersonalCounter : ScannedCounter
    {
        private string user;

        public string User
        {
            get { return user; }
            set { user = value; }
        }

    }
}
