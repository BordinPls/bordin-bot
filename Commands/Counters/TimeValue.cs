﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BordinBot.Commands.Counters
{
    /// <summary>
    /// A CounterValue whose count represents the amount of minutes that have passed since
    /// this value was last updated.
    /// </summary>
    /// <seealso cref="BordinBot.Commands.Counters.CounterValue" />
    public class TimeValue : CounterValue
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CounterValue"/> class.
        /// Initializes value to 0 and owner to empty string.
        /// </summary>
        public TimeValue()
        {
            mType = ECounterValueType.Date;
            mLastUpdateDate = DateTime.Now;
            mValue = 0;
            mOwner = "";
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CounterValue"/> class with a given
        /// owner and initial value.
        /// </summary>
        /// <param name="value">Initial value.</param>
        /// <param name="owner">Owner/thing associated with this value, i.e. "Bordin".</param>
        public TimeValue(uint value, string owner)
        {
            mType = ECounterValueType.Date;
            mLastUpdateDate = DateTime.Now;
            Value = value;
            User = owner;
        }

        /// <summary>
        /// On Get, returns the minutes that have passed between the last Get.
        /// On Set, sets the Value to 0 always, and sets the last update date to this
        /// moment in time.
        /// </summary>
        public override uint Value
        {
            get
            {
                return mValue;
            }
            set
            {
                mValue = 0;
                mLastUpdateDate = DateTime.Now;
            }
        }

        /// <summary>
        /// Last time this value was updated.
        /// </summary>
        private DateTime mLastUpdateDate = DateTime.Now;

        /// <summary>
        /// Gets or sets the last time this value was updated.
        /// </summary>
        public DateTime LastUpdateDate
        {
            get { return mLastUpdateDate; }
            set { mLastUpdateDate = value; }
        }


        /// <summary>
        /// Returns the time since this value was updated. Also prompts an update of the 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            var span = DateTime.Now - LastUpdateDate;
            var builder = new StringBuilder();

            // Append user, if any
            if (User != "")
            {
                builder.Append($"{User}:\t\t");
            }

            // Catch edge case early
            if (span.TotalMinutes < 1)
            {
                builder.Append("Less than a minute ");
                return builder.ToString();
            }

            // Add times
            if (span.Days > 0)
            {
                builder.Append($"{span.Days} Day{(span.Days > 1 ? 's' : "")} ");
            }

            if (span.Hours > 0)
            {
                builder.Append($"{span.Hours} Hour{(span.Hours > 1 ? 's' : "")} ");
            }

            if (span.Minutes > 0)
            {
                builder.Append($"{span.Minutes} Minute{(span.Minutes > 1 ? 's' : "")} ");
            }

            return builder.ToString();
        }
    }
}
