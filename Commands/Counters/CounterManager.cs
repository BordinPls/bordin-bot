﻿using System;
using System.Collections.Generic;
using BordinBot.Util.Logging;
using DSharpPlus.CommandsNext;

namespace BordinBot.Commands.Counters
{
    /// <summary>
    /// Maintains a list of all existing counters and performs operations for their alteration and display.
    /// </summary>
    class CounterManager
    {

        /// <summary>
        /// List of all active Counters, with Name -> Counter. Name is case insensitive.
        /// </summary>
        private Dictionary<string, Counter> mCounters = new Dictionary<string, Counter>();

        /// <summary>
        /// Writes and loads all Counters.
        /// </summary>
        private CounterFileReaderWriter mCounterFileWriter = new CounterFileReaderWriter();

        public CounterManager()
        {
            // Load Counters
            try
            {
                mCounters = mCounterFileWriter.LoadFile();
            }
            catch (Exception e)
            {
                Logger.WriteLine("Failed to load Counters: {0}", Microsoft.Extensions.Logging.LogLevel.Warning, e.Message);
            }
        }

        /// <summary>
        /// Returns a list of all loaded counters.
        /// </summary>
        /// <returns>List of (Counter Name, Description). If none are loaded, returns an empty list.</returns>
        public List<(string, string)> GetAllCounters()
        {
            var returnList = new List<(string, string)>();
            foreach (var counter in mCounters)
            {
                returnList.Add((counter.Key, counter.Value.Description));
            }

            return returnList;
        }

        /// <summary>
        /// Gets the title, description, and value for a Counter.
        /// </summary>
        /// <param name="counterName">Name of the counter.</param>
        /// <param name="context">Discord context this was requested in. Necessary as some Counters need to see messages.</param>
        /// <returns>
        /// Structure containing Counter Name, Description, Value(s)
        /// </returns>
        /// <exception cref="CounterNotFoundException">no counter with given name exists</exception>
        public Counter.CounterStatus GetCounterStatus(in string counterName, CommandContext context)
        {
            var loweredName = counterName.ToLower();

            // if counter doesn't exist, throw
            if (!CounterExists(loweredName))
                throw new CounterNotFoundException($"no counter with name {loweredName} exists");

            // Pull and return info
            var counter = mCounters[loweredName];
            var status = counter.GetCounterStatus(context);

            // If it's a scanned counter, it will trigger an update of values, and thus the file must be resaved
            if (counter.Type == ECounterType.Scanned)
                OnCounterChange();

            return status;
        }

        /// <summary>
        /// Increases the value for a Counter by 1. If a value owner is given, will increment
        /// that value, if found on the counter.
        /// </summary>
        /// <param name="counterName">Name of the counter.</param>
        /// <param name="valueOwner">Owner of the value to increment.</param>
        /// <returns>
        /// Structure containing Counter Name, Description, Value(s)
        /// </returns>
        /// <exception cref="CounterNotFoundException">no counter with given name exists</exception>
        public void IncrementCounter(in string counterName, in string valueOwner = "")
        {
            var loweredName = counterName.ToLower();

            // if counter doesn't exist, throw
            if (!CounterExists(loweredName))
                throw new CounterNotFoundException($"no counter with name {loweredName} exists");

            // Increment value based on whether there was an owner given
            var counter = mCounters[loweredName];
            counter.IncrementValue(valueOwner);

            // Save
            OnCounterChange();
        }

        /// <summary>
        /// Lowers the value for a Counter by 1.
        /// </summary>
        /// <param name="counterName">Name of the counter.</param>
        /// <param name="valueOwner">Owner of the value to increment.</param>
        /// <returns></returns>
        /// <exception cref="BordinBot.Commands.Counters.CounterManager.CounterNotFoundException">no counter with name {loweredName} exists</exception>
        /// <exception cref="CounterNotFoundException">no counter with given name exists</exception>
        public void DecrementCounter(in string counterName, in string valueOwner = "")
        {
            var loweredName = counterName.ToLower();

            // if counter doesn't exist, throw
            if (!CounterExists(loweredName))
                throw new CounterNotFoundException($"no counter with name {loweredName} exists");

            // Decrement value based on whether there was an owner given
            var counter = mCounters[loweredName];
            counter.DecrementValue(valueOwner);

            // Save
            OnCounterChange();
        }

        /// <summary>
        /// Sets the value of a Counter.
        /// </summary>
        /// <param name="counterName">Name of the Counter.</param>
        /// <param name="value">The value for the Counter.</param>
        /// <param name="valueOwner">The value owner.</param>
        /// <exception cref="CounterNotFoundException">no counter with the name exists</exception>
        public void SetCounterValue(in string counterName, in uint value, in string valueOwner = "")
        {
            var loweredName = counterName.ToLower();

            // check if counter exists
            if (!CounterExists(loweredName))
                throw new CounterNotFoundException($"no counter with name {loweredName} exists");

            // Set value
            mCounters[loweredName].SetValue(value, valueOwner);
            OnCounterChange();
        }

        /// <summary>
        /// Returns whether a Counter with counterName exists in the Counter list.
        /// </summary>
        /// <param name="counterName">Name of the counter. Case insensitive.</param>
        /// <returns>True if a Counter with that name was found; false otherwise.</returns>
        private bool CounterExists(in string counterName)
        {
            var loweredName = counterName.ToLower();
            return mCounters.ContainsKey(loweredName);
        }

        /// <summary>
        /// Creates a new ManualCounter with a name and description.
        /// </summary>
        /// <param name="name">The name to identify the Counter with.</param>
        /// <param name="description">A description for what this Counter is for, i.e. "Number of times x has happened".</param>
        /// <exception cref="System.InvalidOperationException">counter with name already exists; cannot create new one</exception>
        public void AddCounter(in string name, in string description)
        {
            if (CounterExists(name))
            {
                throw new InvalidOperationException($"counter with name {name} already exists; cannot create new one");
            }

            var loweredName = name.ToLower();
            Counter newCounter = new ManualCounter(loweredName, description);
            mCounters[loweredName] = newCounter;

            OnCounterChange();
        }

        /// <summary>
        /// Creates a new ScannedCounter with a name, description, and string to search messages for.
        /// </summary>
        /// <param name="counterName">The name to identify the Counter with.</param>
        /// <param name="description">A description for what this Counter is for, i.e. "Number of times x has happened".</param>
        /// <param name="searchString">String to search all messages with.</param>
        /// <exception cref="System.InvalidOperationException">counter with name {name} already exists; cannot create new one</exception>
        public void AddCounter(in string name, in string description, in string searchString)
        {
            if (CounterExists(name))
            {
                throw new InvalidOperationException($"counter with name {name} already exists; cannot create new one");
            }

            var loweredName = name.ToLower();
            Counter newCounter = new ScannedCounter(name, description, searchString);
            mCounters[loweredName] = newCounter;

            OnCounterChange();
        }

        public void RemoveCounter(in string counterName)
        {
            if (!CounterExists(counterName))
            {
                throw new CounterNotFoundException($"counter with name '{counterName}' does not exist and cannot be deleted");
            }

            // Remove and save
            mCounters.Remove(counterName);
            OnCounterChange();
        }

        /// <summary>
        /// Adds a new CounterValue of a given type to a Counter.
        /// </summary>
        /// <param name="counterName">Name of the counter to add a value to.</param>
        /// <param name="valueType">Type of the value to add. Can be "normal" or "time".</param>
        /// <param name="user">The user associated with this value.</param>
        /// <param name="initialValue">The initial value to set.</param>
        public void AddCounterValue(in string counterName, string valueType, in string user = "", in uint initialValue = 0)
        {
            // If an invalid valueType was given, throw
            if (!(valueType == "normal" || valueType == "date"))
            {

            }

            // If no such Counter exists, throw
            if (!CounterExists(counterName))
            {
                throw new CounterNotFoundException($"counter with name {counterName} not found");
            }

            // Create a new value for the type
            CounterValue newValue;

            switch (valueType)
            {
                case "normal":
                    newValue = new CounterValue(initialValue, user);
                    break;
                case "time":
                    newValue = new TimeValue(initialValue, user);
                    break;
                default:
                    throw new ArgumentException($"Given value type '{valueType}' is not a valid option");
            }

            // Add the value
            var loweredCounterName = counterName.ToLower();
            mCounters[loweredCounterName].AddNewValue(newValue);

            // Save
            OnCounterChange();
        }

        /// <summary>
        /// Called when a Counter changes value, or when a Counter is added or removed from the list.
        /// Prompts a save of the CounterFile.
        /// </summary>
        private void OnCounterChange()
        {
            mCounterFileWriter.WriteCounterFile(mCounters);
        }


        /// <summary>
        /// Indicates that a Counter was sought for an operation but was not found.
        /// </summary>
        /// <seealso cref="System.Exception" />
        [Serializable]
        public class CounterNotFoundException : Exception
        {
            public CounterNotFoundException() { }
            public CounterNotFoundException(string message) : base(message) { }
            public CounterNotFoundException(string message, Exception inner) : base(message, inner) { }
            protected CounterNotFoundException(
              System.Runtime.Serialization.SerializationInfo info,
              System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
        }
    }
}
