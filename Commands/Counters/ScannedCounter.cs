﻿using System;
using System.Collections.Generic;
using System.Text;
using DSharpPlus.CommandsNext;
using DSharpPlus.Entities;

namespace BordinBot.Commands.Counters
{
    /// <summary>
    /// A Counter class that scans messages for a key phrase (case insensitive) automatically and increments
    /// the Counter when they are discovered.
    /// </summary>
    /// <seealso cref="BordinBot.Commands.Counters.Counter" />
    class ScannedCounter : Counter
    {
        #region Constructor
        public ScannedCounter()
        {
            mPhrase = "";
            Type = ECounterType.Scanned;
        }

        /// <summary>
        /// Constructs a ScannedCounter with the desired parameters.
        /// </summary>
        /// <param name="name">Name of this counter.</param>
        /// <param name="description">Description for this counter.</param>
        /// <param name="phrase">Key phrase to search messages for. Will be lowered when saved to maintain case insensitivity.</param>
        public ScannedCounter(in string name, in string description, in string phrase)
        {
            mName = name;
            mDescription = description;
            mPhrase = phrase.ToLower();
            Type = ECounterType.Scanned;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Number of Discord messages, at most, to scan back through history for.
        /// </summary>
        protected const int MAX_MESSAGES = 5000;

        /// <summary>
        /// Key phrase to search messages for in order to increment values. Should be lowercase.
        /// </summary>
        protected string mPhrase;

        /// <summary>
        /// Gets or sets the key phrase to search messages for in order to increment values. Will
        /// lowercase the value that is set.
        /// </summary>
        /// <value>
        /// The phrase to set.
        /// </value>
        public virtual string Phrase
        {
            get { return mPhrase; }
            set { mPhrase = value.ToLower(); }
        }

        /// <summary>
        /// ID of DiscordMessage that will be the starting point of the next scan of messages. Set to the ID
        /// of the last message
        /// </summary>
        private ulong mNextScanStartMessageID = 0;

        /// <summary>
        /// Gets or sets time up to which messages were most recently scanned. Acts as a reference point for how far back
        /// to search in the Discord message history for phrases.
        /// </summary>
        public ulong NextScanStartMessageID
        {
            get { return mNextScanStartMessageID; }
            set { mNextScanStartMessageID = value; }
        }

        /// <summary>
        /// Identifier for the discord channel that this will scan. Will be set to the first channel that 
        /// this counter is asked the count from.
        /// </summary>
        private ulong mChannelID = 0;

        /// <summary>
        /// Gets the identifier for the discord channel that this counter will scan.
        /// </summary>
        public ulong ChannelID
        {
            get { return mChannelID; }
            set { mChannelID = value; }
        }

        #endregion

        #region Methods

        protected override string PrintValues()
        {
            // Throw if no values
            if (mValues.Count < 1)
            {
                throw new CounterValueNotFoundException("This counter does not have any values to print");
            }

            // Append stringbuilder with every value's output
            StringBuilder builder = new StringBuilder();
            foreach (var keyvalpair in mValues)
            {
                // For scanned messages, it's less ugly and makes
                // more sense to equate "" with "Everybody"
                if (keyvalpair.Key == "")
                    builder.AppendLine($"Everybody: {keyvalpair.Value.Value}");
                else
                    builder.AppendLine(keyvalpair.Value.ToString());
            }

            return builder.ToString();
        }

        /// <summary>
        /// Returns a string representation of all of the values counted by this counter. This will trigger
        /// a scan of all messages since the last scan date (up to the message limit set on this class) and an update
        /// of all values for the keyphrase on this counter.
        /// </summary>
        /// <param name="context">Discord Context in which to scan messages.</param>
        /// <returns>String representation of all values, with each value on a line.</returns>
        /// <exception cref="CounterValueNotFoundException"></exception>
        protected override string Count(CommandContext context)
        {
            // Throw if no values
            if (mValues.Count < 1)
            {
                throw new CounterValueNotFoundException("This counter does not have any values to count");
            }

            // Scan all messages to update counters based on current values
            // Will be blocking but too hard to be asynchronous with this stuff while
            // acting as expected
            ScanMessages(context);

            // Since it's gotta scan asynchronously, just return a placeholder for the value counts
            // It will still reply to 
            return PrintValues();
        }

        /// <summary>
        /// Returns a list of DiscordMessages for the channel in a given Discord context. Will only scan up to
        /// maxMessages to avoid blocking the bot for too long.
        /// </summary>
        /// <param name="channel">Channel in which to scan messages.</param>
        /// <param name="maxMessages">Maximum number of messages to scan through. Defaults to MAX_MESSAGES.</param>
        /// <returns>ReadOnlyList of messages within the channel.</returns>
        protected IReadOnlyList<DiscordMessage> GetMessages(in DiscordChannel channel, in int maxMessages = MAX_MESSAGES)
        {
            var task = channel.GetMessagesAsync(maxMessages);
            task.Wait();
            return task.Result;
        }

        /// <summary>
        /// Returns a list of DiscordMessages for the channel in a given Discord context. Will only scan up to
        /// maxMessages to avoid blocking the bot for too long.
        /// </summary>
        /// <param name="channel">Channel in which to scan messages.</param>
        /// <param name="lastMessageID">Message ID to scan forward from.</param>
        /// <param name="maxMessages">Maximum number of messages to scan through. Defaults to MAX_MESSAGES.</param>
        /// <returns>ReadOnlyList of messages within the channel.</returns>
        protected IReadOnlyList<DiscordMessage> GetMessages(in DiscordChannel channel, in ulong lastMessageID, in int maxMessages = MAX_MESSAGES)
        {
            var task = channel.GetMessagesAfterAsync(lastMessageID);
            task.Wait();
            return task.Result;
        }

        /// <summary>
        /// Scans messages in the given context and updates all values on this counter.
        /// </summary>
        /// <param name="context"></param>
        protected virtual void ScanMessages(CommandContext context)
        {
            // If the current channel for this counter isn't set, set it to this one
            if (mChannelID == 0)
            {
                mChannelID = context.Channel.Id;
            }
            // Else, the channel is already set; check if this was called from the channel associated with this counter
            else
            {
                if (mChannelID != context.Channel.Id)
                {
                    throw new WrongChannelException("This is not the correct channel to call this counter from");
                }
            }

            // Grab the message history
            IReadOnlyList<DiscordMessage> messages;
            // If the last message was set, scan from that point
            if (mNextScanStartMessageID != 0)
            {
                messages = GetMessages(context.Channel, mNextScanStartMessageID);
            }
            // Else, this the first scan and should move backwards from here
            else
            {
                messages = GetMessages(context.Channel);
            }

            // Finally, update all values
            UpdateValuesFromMessages(messages);
            mNextScanStartMessageID = context.Message.Id;
        }

        /// <summary>
        /// Scans a list of messages for the keyphrase stored on this counter. Will skip messages that don't
        /// have a user that goes with the values on this counter, and will skip any messages from users flagged
        /// as bots.
        /// </summary>
        /// <param name="messages">List of messages to search through.</param>
        protected void UpdateValuesFromMessages(in IReadOnlyList<DiscordMessage> messages)
        {
            // Build a Hashset of the names to look for to make this faster
            HashSet<string> users = new HashSet<string>();
            foreach (var valuePair in mValues)
            {
                users.Add(valuePair.Key);
            }

            // For each message, scan for phrase
            foreach (var message in messages)
            {
                // Because Discord messages, for some reason, format the user in messages with an exclamation point,
                // gotta remove it to check the author
                int exclamationIndex = message.Author.Mention.IndexOf('!');
                string messageAuthor = message.Author.Mention.Remove(exclamationIndex, 1);

                // First, see if the any of this message applies to any of the users
                // If it doesn't contain a global scanner or the sender of this message, skip
                if (!(users.Contains("") || users.Contains(messageAuthor)))
                    continue;

                // If the message was written by a bot, skip
                if (message.Author.IsBot)
                    continue;

                // Substring to ignore any calls to this bot that may have occurred, and lower it as well
                string cleanedMessage = message.Content;
                if (cleanedMessage.StartsWith(BotTools.Configuration.Prefix))
                {
                    cleanedMessage = cleanedMessage.Substring(BotTools.Configuration.Prefix.Length);
                }
                cleanedMessage = cleanedMessage.ToLower();

                // See if message contains the key phrase, and increment values appropriately
                if (cleanedMessage.Contains(mPhrase))
                {
                    // Increment it for the global first, if present
                    if (users.Contains(""))
                        mValues[""].Increment();

                    // Then increment for the user
                    if (users.Contains(messageAuthor))
                        mValues[messageAuthor].Increment();
                }
            }
        }
        #endregion

        #region Exceptions
        /// <summary>
        /// Thrown when attempting to scan in a context where
        /// </summary>
        [Serializable]
        public class WrongChannelException : Exception
        {
            public WrongChannelException() { }
            public WrongChannelException(string message) : base(message) { }
            public WrongChannelException(string message, Exception inner) : base(message, inner) { }
            protected WrongChannelException(
              System.Runtime.Serialization.SerializationInfo info,
              System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
        }
        #endregion
    }
}
