﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BordinBot.Commands.Counters
{
    /// <summary>
    /// Represents the current value of a counter.
    /// </summary>
    public class CounterValue
    {
        #region properties

        /// <summary>
        /// Type of CounterValue.
        /// </summary>
        protected ECounterValueType mType;

        /// <summary>
        /// Gets or sets the Type of CounterValue.
        /// </summary>
        public ECounterValueType Type
        {
            get { return mType; }
            set { mType = value; }
        }

        /// <summary>
        /// Number of occurrences.
        /// </summary>
        protected uint mValue;

        /// <summary>
        /// Gets or sets the number of Counts for.
        /// </summary>
        /// <value>
        /// The current number for this Count.
        /// </value>
        public virtual uint Value
        {
            get { return mValue; }
            set { mValue = value; }
        }

        /// <summary>
        /// The DiscordUser string this value is associated with.
        /// </summary>
        protected string mOwner;

        /// <summary>
        /// Gets or sets the DiscordUser string this value is associated with.
        /// </summary>
        /// <value>
        /// The owner associated with the value.
        /// </value>
        public string User
        {
            get { return mOwner; }
            set { mOwner = value; }
        }

        #endregion

        #region methods

        /// <summary>
        /// Initializes a new instance of the <see cref="CounterValue"/> class.
        /// Initializes value to 0 and owner to empty string.
        /// </summary>
        public CounterValue()
        {
            mType = ECounterValueType.Integer;
            mValue = 0;
            mOwner = "";
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CounterValue"/> class with a given
        /// owner and initial value.
        /// </summary>
        /// <param name="value">Initial value.</param>
        /// <param name="owner">Owner/thing associated with this value, i.e. "Bordin".</param>
        public CounterValue(uint value, string owner)
        {
            Value = value;
            User = owner;
        }



        /// <summary>
        /// Increments this Counter's number by 1.
        /// </summary>
        public virtual void Increment() { if (Value != uint.MaxValue) Value++; }
        /// <summary>
        /// Decrements this Counter's number by 1.
        /// </summary>
        public virtual void Decrement() { if (Value != uint.MinValue) Value--; }

        /// <summary>
        /// Converts to string.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            // If there's no associated user, just dump the number out 
            if (User == "")
            {
                return Value.ToString();
            }
            else
            {
                return $"{User}:    {Value.ToString()}";
            }
        }
        #endregion
    }
}
