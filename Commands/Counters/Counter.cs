﻿using System;
using System.Collections.Generic;
using System.Text;
using DSharpPlus.CommandsNext;

namespace BordinBot.Commands.Counters
{
    /// <summary>
    /// A maintained number of occurrences that's tracked across the server.
    /// </summary>
    public class Counter
    {
        /// <summary>
        /// Readonly struct containing status of Counter..
        /// </summary>
        public readonly struct CounterStatus
        {
            public CounterStatus(string name, string description, string value) => (Name, Description, Value) = (name, description, value);

            public string Name { get; init; }
            public string Description { get; init; }
            public string Value { get; init; }
        }

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="Counter"/> class. Type is set to Manual by default.
        /// </summary>
        public Counter() { Type = ECounterType.Manual; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Counter"/> class with the given parameters.
        /// </summary>
        /// <param name="name">Name of this counter. Used to identify and call upon counters.</param>
        /// <param name="description">Description of this counter. Displayed text when count is called up, i.e. "Number of times x has happened"</param>
        /// <param name="type">ECounterType for this counter.</param>
        public Counter(string name, string description, ECounterType type)
        {
            Name = name;
            Description = description;
            Type = type;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Name identifier for this Counter. This is used to call up the Counter
        /// number in the server.
        /// </summary>
        protected string mName;

        /// <summary>
        /// Gets or sets the Name.
        /// </summary>
        /// <value>
        /// The Name.
        /// </value>
        public string Name
        {
            get { return mName; }
            set { mName = value; }
        }

        /// <summary>
        /// Description of this counter. Displayed text when count is called up, i.e. "Number of times x has happened"
        /// </summary>
        protected string mDescription;

        /// <summary>
        /// Description of this counter. Displayed text when count is called up, i.e. "Number of times x has happened"
        /// </summary>
        public virtual string Description
        {
            get { return mDescription; }
            set { mDescription = value; }
        }

        /// <summary>
        /// ECounterType that describes this.
        /// </summary>
        protected ECounterType mType;

        /// <summary>
        /// Gets or sets the type of Counter.
        /// </summary>
        /// <value>
        /// Describes the type of Counter..
        /// </value>
        public ECounterType Type
        {
            get { return mType; }
            set { mType = value; }
        }


        /// <summary>
        /// Dictionary of User name -> CounterValue that this Counter tracks.
        /// </summary>
        protected Dictionary<string, CounterValue> mValues = new Dictionary<string, CounterValue>();

        public Dictionary<string, CounterValue> Values
        {
            get { return mValues; }
            set { mValues = value; }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Adds a new value for this Counter to keep track of.
        /// </summary>
        /// <param name="value">CounterValue to add to this Counter.</param>
        public virtual void AddNewValue(in CounterValue value)
        {
            // If the Counter already has a value with that user, throw
            if (mValues.TryGetValue(value.User, out _))
            {
                throw new DuplicateCounterValueException($"Counter already has a value with user '{value.User}'");
            }
            // Else, add to list
            else
            {
                mValues.Add(value.User, value);
            }
        }

        /// <summary>
        /// Increments a CounterValue with the given owner by one.
        /// </summary>
        /// <param name="valueOwner">Name associated with the value.</param>
        /// <exception cref="BordinBot.Commands.Counters.Counter.CounterValueNotFoundException">This counter does not have any values to increment</exception>
        public virtual void IncrementValue(string valueOwner)
        {
            // If there's no values on this counter, throw
            if (mValues.Count < 1)
            {
                throw new CounterValueNotFoundException("This counter does not have any values to increment");
            }

            // Attempt to find a value with that owner
            // Try to find a CounterValue with this owner
            CounterValue value;
            if (mValues.TryGetValue(valueOwner, out value))
            {
                value.Increment();
            }
            else
            {
                throw new CounterValueNotFoundException($"Counter {Name} has no value with the user {valueOwner}.");
            }
        }

        /// <summary>
        /// Decrements a CounterValue with the given owner by one. 
        /// </summary>
        /// <param name="valueOwner">Name associated with the value.</param>
        /// <exception cref="BordinBot.Commands.Counters.Counter.CounterValueNotFoundException">This counter does not have any values to increment</exception>
        public virtual void DecrementValue(string valueOwner)
        {
            // If there's no values on this counter, throw
            if (mValues.Count < 1)
            {
                throw new CounterValueNotFoundException("This counter does not have any values to decrement");
            }

            // Try to find a CounterValue with this owner
            CounterValue value;
            if (mValues.TryGetValue(valueOwner, out value))
            {
                value.Decrement();
            }
            else
            {
                throw new CounterValueNotFoundException($"Counter {Name} has no value with the user {valueOwner}.");
            }
        }

        /// <summary>
        /// Sets a CounterValue to a specific number. If no value owner is given, will attempt to set the value
        /// of the first CounterValue in this counter. Throws a CounterValueNotFoundException if there are no
        /// values or one with the given owner is not found.
        /// </summary>
        /// <param name="newValue">New value to set the CounterValue to.</param>
        public void SetValue(in uint newValue, in string valueOwner = "")
        {
            // Throw if no values
            if (mValues.Count < 1) { throw new CounterValueNotFoundException($"Counter {Name} has no values currently."); }

            // Try to find a CounterValue with this owner
            CounterValue value;
            if (mValues.TryGetValue(valueOwner, out value))
            {
                value.Value = newValue;
            }
            else
            {
                throw new CounterValueNotFoundException($"Counter {Name} has no value with the user {valueOwner}.");
            }

            value.Value = newValue;
        }

        /// <summary>
        /// Returns a string representation of the value(s) of this Counter. If the Counter has no values,
        /// throws a CounterValueNotFoundException.
        /// </summary>
        /// <returns>The value for this counter, or a list of all of its values.</returns>
        /// <exception cref="CounterValueNotFoundException"></exception>
        protected virtual string Count(CommandContext context)
        {
            // Throw if no values
            if (mValues.Count < 1)
            {
                throw new CounterValueNotFoundException("This counter does not have any values to count");
            }

            // Print all vals
            return PrintValues();
        }

        /// <summary>
        /// Returns the Name, Description, and Count for this Counter.
        /// </summary>
        /// <returns>CounterStatus that describes this Counter and its state.</returns>
        public CounterStatus GetCounterStatus(CommandContext context)
        {
            return new CounterStatus(Name, Description, Count(context));
        }

        protected virtual string PrintValues()
        {
            // Throw if no values
            if (mValues.Count < 1)
            {
                throw new CounterValueNotFoundException("This counter does not have any values to print");
            }

            // Append stringbuilder with every value's output
            StringBuilder builder = new StringBuilder();
            foreach (var keyvalpair in mValues)
            {
                builder.AppendLine(keyvalpair.Value.ToString());
            }

            return builder.ToString();
        }
        #endregion

        #region Overrides

        /// <summary>
        /// Determines whether the specified <see cref="System.Object" />, is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
        /// <returns>
        ///   <c>true</c> if the specified <see cref="System.Object" /> is a <see cref="BordinBot.Commands.Counters"/>; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            return obj is Counter counter &&
                   mName == counter.mName &&
                   Name == counter.Name;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(mName);
        }

        #endregion
        #region Exceptions
        /// <summary>
        /// Thrown when attempting to icnrease a CounterValue that doesn't exist or a CounterValue with a 
        /// specified owner was not found
        /// </summary>
        [Serializable]
        public class CounterValueNotFoundException : Exception
        {
            public CounterValueNotFoundException() { }
            public CounterValueNotFoundException(string message) : base(message) { }
            public CounterValueNotFoundException(string message, Exception inner) : base(message, inner) { }
            protected CounterValueNotFoundException(
              System.Runtime.Serialization.SerializationInfo info,
              System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
        }

        /// <summary>
        /// Thrown when attempting to add a CounterValue with a user that matches an existing value on this Counter.
        /// </summary>
        [Serializable]
        public class DuplicateCounterValueException : Exception
        {
            public DuplicateCounterValueException() { }
            public DuplicateCounterValueException(string message) : base(message) { }
            public DuplicateCounterValueException(string message, Exception inner) : base(message, inner) { }
            protected DuplicateCounterValueException(
              System.Runtime.Serialization.SerializationInfo info,
              System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
        }
        #endregion
    }
}
