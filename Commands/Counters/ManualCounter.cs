﻿namespace BordinBot.Commands.Counters
{
    /// <summary>
    /// Counter where its value(s) must be manually incremented/decremented.
    /// </summary>
    public class ManualCounter : Counter
    {
        public ManualCounter()
        {
            Type = ECounterType.Manual;
        }

        public ManualCounter(string name, string description)
        {
            Name = name;
            Description = description;
            Type = ECounterType.Manual;
        }
    }
}
