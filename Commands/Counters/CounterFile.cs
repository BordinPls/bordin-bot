﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text.Json;
using System.Text.Json.Serialization;
using BordinBot.Util.Logging;

namespace BordinBot.Commands.Counters
{
    /// <summary>
    /// Provides functions for reading and writing a file of Counters.
    /// </summary>
    class CounterFileReaderWriter
    {


        /// <summary>
        /// The name of the Counters file.
        /// </summary>
        private const string mFileName = "Counters.json";

        public CounterFileReaderWriter()
        {

        }

        /// <summary>
        /// Writes a collection of Counters to the desired location.
        /// </summary>
        /// <param name="path">Absolute path to the write location of the file.</param>
        public void WriteCounterFile(Dictionary<string, Counter> counterList)
        {
            if (counterList == null)
                throw new ArgumentNullException(nameof(counterList));

            // Get and check the target directory
            string targetDirectory = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "Data/");

            // If the target directory does not exist...
            if (!Directory.Exists(targetDirectory))
            {
                // Attempt to make a new directory
                Directory.CreateDirectory(targetDirectory);
            }

            //Get file to write to
            string targetPath = Path.Combine(targetDirectory, mFileName);

            // Create serializer
            var options = new JsonSerializerOptions
            {
                WriteIndented = true,
                Converters =
                {
                    new CounterConverter(),
                    new CounterValueConverter()
                }
            };

            // Serialize objects
            var jsonString = JsonSerializer.Serialize<object>(counterList, options);

            // Write to file
            File.WriteAllText(targetPath, jsonString);
        }

        /// <summary>
        /// Loads the Counters.json file and returns all read Counters.
        /// </summary>
        /// <returns>Dictionary of CounterName -> Counter read from file.</returns>
        /// <exception cref="System.IO.FileNotFoundException">Counters.json file not found</exception>
        public Dictionary<string, Counter> LoadFile()
        {
            // Get and check if the file exists
            string targetPath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "Data/", mFileName);
            // If the target directory does not exist...
            if (!File.Exists(targetPath))
            {
                throw new FileNotFoundException("Counters.json file not found");
            }

            // Read json string from file
            string jsonstring = File.ReadAllText(targetPath);
            JsonSerializerOptions jsonOptions = new JsonSerializerOptions();
            jsonOptions.Converters.Add(new CounterConverter());
            jsonOptions.Converters.Add(new CounterValueConverter());
            Dictionary<string, Counter> loadedCounters = JsonSerializer.Deserialize<Dictionary<String, Counter>>(jsonstring, jsonOptions);
            Logger.WriteLine($"Successfully read from {mFileName}: {loadedCounters.Count} Counters loaded");
            return loadedCounters;

        }

        #region JsonConverters

        /// <summary>
        /// Custom converter for dealing with the fact I use Polymorphism with the list of Counters, and the JSON library
        /// doesn't inherently support the serialization and deserialization of those classes.
        /// </summary>
        /// <seealso cref="System.Text.Json.Serialization.JsonConverter&lt;BordinBot.Commands.Counters.Counter&gt;" />
        private class CounterConverter : JsonConverter<Counter>
        {
            public override void Write(Utf8JsonWriter writer, Counter counter, JsonSerializerOptions options)
            {
                writer.WriteStartObject();

                // Write properties
                writer.WriteString("Type", counter.Type.ToString());
                writer.WriteString("Name", counter.Name);
                writer.WriteString("Description", counter.Description);

                // If scanned counter, add in phrase
                if (counter.Type == ECounterType.Scanned)
                {
                    writer.WriteString("Phrase", (counter as ScannedCounter).Phrase);
                    writer.WriteNumber("LastMessageID", (counter as ScannedCounter).NextScanStartMessageID);
                    writer.WriteNumber("ChannelID", (counter as ScannedCounter).ChannelID);
                }

                writer.WritePropertyName("Values");
                JsonSerializer.Serialize(writer, counter.Values, typeof(Dictionary<string, CounterValue>), options);

                writer.WriteEndObject();
            }

            public override Counter Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
            {
                // Copied from: https://docs.microsoft.com/en-us/dotnet/standard/serialization/system-text-json-converters-how-to?pivots=dotnet-6-0#support-polymorphic-deserialization

                if (reader.TokenType != JsonTokenType.StartObject)
                {
                    throw new JsonException();
                }

                reader.Read();
                if (reader.TokenType != JsonTokenType.PropertyName)
                {
                    throw new JsonException();
                }
#nullable enable
                string? propertyName = reader.GetString();
                if (propertyName == null)
                    throw new JsonException();
#nullable disable
                if (propertyName != "Type")
                {
                    throw new JsonException();
                }

                reader.Read();
                if (reader.TokenType != JsonTokenType.String)
                {
                    throw new JsonException();
                }

                // Parse Enum type from string
                ECounterType typeDiscriminator;
                if (!Enum.TryParse(reader.GetString(), out typeDiscriminator))
                    throw new JsonException();

                // Create new Counter based on Type
                Counter counter = typeDiscriminator switch
                {
                    ECounterType.Manual => new ManualCounter(),
                    ECounterType.Scanned => new ScannedCounter(),
                    _ => throw new JsonException()
                };

                while (reader.Read())
                {
                    if (reader.TokenType == JsonTokenType.EndObject)
                    {
                        return counter;
                    }

                    if (reader.TokenType == JsonTokenType.PropertyName)
                    {
                        propertyName = reader.GetString();
                        reader.Read();
                        switch (propertyName)
                        {
                            case "Name":
                                string name = reader.GetString();
                                counter.Name = name;
                                break;
                            case "Description":
                                string desc = reader.GetString();
                                counter.Description = desc;
                                break;
                            case "Phrase":
                                string phrase = reader.GetString();
                                ((ScannedCounter)counter).Phrase = phrase;
                                break;
                            case "Values":
                                counter.Values = JsonSerializer.Deserialize<Dictionary<string, CounterValue>>(ref reader, options);
                                break;
                            case "ChannelID":
                                ulong channelId = reader.GetUInt64();
                                ((ScannedCounter)counter).ChannelID = channelId;
                                break;
                            case "LastMessageID":
                                ulong messageId = reader.GetUInt64();
                                ((ScannedCounter)counter).NextScanStartMessageID = messageId;
                                break;
                        }
                    }
                }

                throw new JsonException();
            }
        }

        /// <summary>
        /// Custom converter for dealing with the fact I use Polymorphism with the CounterValues, and the JSON library
        /// doesn't inherently support the serialization and deserialization of those classes.
        /// </summary>
        /// <seealso cref="System.Text.Json.Serialization.JsonConverter&lt;BordinBot.Commands.Counters.Counter&gt;" />
        private class CounterValueConverter : JsonConverter<CounterValue>
        {
            public override void Write(Utf8JsonWriter writer, CounterValue counterValue, JsonSerializerOptions options)
            {
                writer.WriteStartObject();

                // Write properties
                writer.WriteString("Type", counterValue.Type.ToString());
                writer.WriteNumber("Value", counterValue.Value);
                writer.WriteString("User", counterValue.User);

                // If timed value, add in lastupdatedate
                if (counterValue.Type == ECounterValueType.Date)
                {
                    writer.WriteString("LastUpdateDate", (counterValue as TimeValue).LastUpdateDate.ToString());
                }

                writer.WriteEndObject();
            }

            public override CounterValue Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
            {
                // Copied from: https://docs.microsoft.com/en-us/dotnet/standard/serialization/system-text-json-converters-how-to?pivots=dotnet-6-0#support-polymorphic-deserialization
                Utf8JsonReader readerClone = reader;
                if (readerClone.TokenType != JsonTokenType.StartObject)
                {
                    throw new JsonException();
                }

                readerClone.Read();
                if (readerClone.TokenType != JsonTokenType.PropertyName)
                {
                    throw new JsonException();
                }
#nullable enable
                string? propertyName = readerClone.GetString();
                if (propertyName == null)
                    throw new JsonException();
#nullable disable
                if (propertyName != "Type")
                {
                    throw new JsonException();
                }

                readerClone.Read();
                if (readerClone.TokenType != JsonTokenType.String)
                {
                    throw new JsonException();
                }

                // Parse Enum type from string
                ECounterValueType typeDiscriminator;
                if (!Enum.TryParse(readerClone.GetString(), out typeDiscriminator))
                    throw new JsonException();

                // Create new CounterValue based on Type
                CounterValue counterValue = typeDiscriminator switch
                {
                    ECounterValueType.Integer => new CounterValue(),
                    ECounterValueType.Date => new TimeValue(),
                    _ => throw new JsonException()
                };

                while (reader.Read())
                {
                    if (reader.TokenType == JsonTokenType.EndObject)
                    {
                        return counterValue;
                    }

                    if (reader.TokenType == JsonTokenType.PropertyName)
                    {
                        propertyName = reader.GetString();
                        reader.Read();
                        switch (propertyName)
                        {
                            case "Value":
                                uint val = reader.GetUInt32();
                                counterValue.Value = val;
                                break;
                            case "User":
                                string user = reader.GetString();
                                counterValue.User = user;
                                break;
                            case "LastUpdateDate":
                                ((TimeValue)counterValue).LastUpdateDate = DateTime.Parse(reader.GetString());
                                break;
                        }
                    }
                }


                throw new JsonException();
            }
        }
        #endregion
    }
}
