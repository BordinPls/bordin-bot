﻿namespace BordinBot.Commands.Counters
{
    public enum ECounterType
    {
        Manual, /// A Counter that must be manually incremented/decremented by a command
        Scanned, /// A Counter with values that will be set by scanned key phrases in server messages
    }

    public enum ECounterValueType
    {
        Integer, /// Simple integer value that represents a count.
        Date /// Value that represents minutes since the count for this value was last updated/retrieved.
    }
}
