﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace BordinBot.Commands
{
    /// <summary>
    /// Commands for random fun things.
    /// </summary>
    class FunCommands : BaseCommandModule
    {
        /// <summary>
        /// Pulls from a poorly inspiring image from the InspiroBot web API.
        /// </summary>
        /// <param name="context">Context in which command was executed.</param>
        /// <returns></returns>
        [Command("inspire")]
        [Description("Provides a wonderfully uplifting image to inspire you to persevere against the crushing weight of existence.")]
        public async Task Inspire(CommandContext context)
        {
            var inspirationalImage = await BotTools.HttpRequestClient.GetStringAsync("https://inspirobot.me/api?generate=true"); // Make request to webpage; spits out image directly
            await context.Channel.SendMessageAsync(inspirationalImage);
        }

        /// <summary>
        /// Pulls a useless Kanye West quote from the Kanye Rest API and displays it in quote format.
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        [Command("kanye")]
        [Description("Provides timeless insight from Yeezus.")]
        public async Task Kanye(CommandContext context)
        {
            // Send HTTP Get request to api
            var requestTask = BotTools.HttpRequestClient.GetStringAsync("https://api.kanye.rest/");
            var responseString = await requestTask;

            // Parse JSON from result string
            JObject obj = JObject.Parse(responseString);
            var quote = (string)obj["quote"];

            // Construct output string for discord block quote
            var sb = new StringBuilder();
            sb.Append("> \"");
            sb.Append(quote);
            sb.Append("\" - Kanye West");
            var outputMessage = sb.ToString();

            // Send off to discord channel
            await context.Channel.SendMessageAsync(outputMessage);

        }

        [Command("trump")]
        [Description("Peer into the complex mind of the 45th president of the United States of America.")]
        public async Task Trump(CommandContext context)
        {
            // Send HTTP Get request to api
            var requestTask = BotTools.HttpRequestClient.GetStringAsync("https://api.whatdoestrumpthink.com/api/v1/quotes/random");
            var responseString = await requestTask;

            // Parse JSON from result string
            JObject obj = JObject.Parse(responseString);
            var quote = (string)obj["message"];

            // Construct output string for discord block quote
            var sb = new StringBuilder();
            sb.Append("> \"");
            sb.Append(quote);
            sb.Append("\"\n - Donald J. Trump, 45th President of the United States of America");
            var outputMessage = sb.ToString();

            // Send off to discord channel
            await context.Channel.SendMessageAsync(outputMessage);
        }

        [Command("trump")]
        [Description("Ask what Donald Trump thinks about someone or something.")]
        public async Task Trump(CommandContext context, string about)
        {
            // Send HTTP Get request to api
            var requestTask = BotTools.HttpRequestClient.GetStringAsync($"https://api.whatdoestrumpthink.com/api/v1/quotes/personalized?q={about}");
            var responseString = await requestTask;

            // Parse JSON from result string
            JObject obj = JObject.Parse(responseString);
            var quote = (string)obj["message"];

            // Construct output string for discord block quote
            var sb = new StringBuilder();
            sb.Append("> \"");
            sb.Append(quote);
            sb.Append("\"\n> - Donald J. Trump, 45th President of the United States of America");
            var outputMessage = sb.ToString();

            // Send off to discord channel
            await context.Channel.SendMessageAsync(outputMessage);
        }

        [Command("eldenbucks")]
        [Description("Convert USD to Elden Rings.")]
        public async Task EldenBucks(CommandContext context, string val)
        {
            decimal conversion;
            var style = NumberStyles.Number | NumberStyles.AllowCurrencySymbol;
            var provider = new CultureInfo("en-US");
            Decimal.TryParse(val, style, provider, out conversion);

            var eldenRings = conversion / (decimal)60.0;

            await context.Channel.SendMessageAsync(String.Format("Man, that's like, {0} Elden Rings!", eldenRings));
        }
    }
}
