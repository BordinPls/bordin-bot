﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
namespace BordinBot.Util.Quotes
{
    public class QuoteFile
    {
        [JsonPropertyName("QuoteCollections")]
        public List<QuoteCollection> QuoteCollections
        {
            get;
            set;
        }

        /// <summary>
        /// If true, all quotes read from this file will be read-only, and cannot have
        /// quotes added or removed. Takes precedence over QuoteCollection read-only flag.
        /// </summary>
        [JsonPropertyName("ReadOnly")]
        public bool ReadOnly { get; set; }

        /// <summary>
        /// Creates a empty QuoteFile with the desired read-only flag.
        /// </summary>
        /// <param name="readOnly">If true, the QuoteFile cannot have </param>
        public QuoteFile(bool readOnly = false)
        {
            QuoteCollections = new List<QuoteCollection>();
            ReadOnly = readOnly;
        }

        /// <summary>
        /// Creates a QuoteFile with the existing QuoteCollections with the desired read-only flag.
        /// </summary>
        /// <param name="inQuoteCollections">QuoteCollections to fill this file with.</param>
        /// <param name="readOnly">If true, the QuoteFile cannot have </param>
        [JsonConstructor]
        public QuoteFile(List<QuoteCollection> QuoteCollections, bool ReadOnly = false)
        {
            this.QuoteCollections = QuoteCollections;
            this.ReadOnly = ReadOnly;
        }

        /// <summary>
        /// Adds a new quote to the collection named quoteCollectionName. Will throw an exception if the collection is not in this file, or the file/collection is read-only.
        /// </summary>
        /// <param name="quote">Quote to add to file.</param>
        /// <param name="quoteCollectionName">Name of collection to add this quote to.</param>
        /// <exception cref="InvalidOperationException">Thrown when attempting to add a quote to a file or collection that is read-only. </exception>
        /// <exception cref="CollectionNotFoundException">Thrown when attempting to add a quote to a collection not in this file.</exception>
        public void AddQuote(in Quote quote, in string quoteCollectionName)
        {
            var name = quoteCollectionName;

            // if the QuoteFile is read only, throw an exception for trying to add in spite of it
            if (ReadOnly)
            {
                throw new InvalidOperationException("Attempted to add a Quote to a file that is read-only.");
            }

            if (!ContainsCollection(quoteCollectionName))
            {
                throw new CollectionNotFoundException(String.Format("collection '{0}' does not exist in this QuoteFile and no quote could be added to it"));
            }

            // Since we have the collection, grab it
            var collection = QuoteCollections.Find(collection => collection.Name == name);
            if (!collection.ReadOnly)
            {
                collection.AddQuote(quote);
            }
            else
            {
                throw new InvalidOperationException(String.Format("attempted to add quote to read-only collection '{0}'", quoteCollectionName));
            }
        }

        /// <summary>
        /// Adds a new collection to this file so it will be saved.
        /// </summary>
        public void AddCollection(in QuoteCollection collection)
        {
            QuoteCollections.Add(collection);
        }

        /// <summary>
        /// Returns true if contains the given QuoteCollection.
        /// </summary>
        /// <param name="collection">QuoteCollection to search for.</param>
        /// <returns>True if the quote collection is stored in this file; false otherwise.</returns>
        public bool ContainsCollection(in QuoteCollection collection)
        {
            return QuoteCollections.Contains(collection);
        }

        /// <summary>
        /// Returns true if the QuoteFile contains a collection with the given name.
        /// </summary>
        /// <param name="collectionName">Name of collection to search for.</param>
        /// <returns>True if a collection with collectionName is in this QuoteFile.</returns>
        public bool ContainsCollection(in string collectionName)
        {
            var name = collectionName;
            var foundCollection = QuoteCollections.Where(collection => collection.Name == name);
            return foundCollection.Count() != 0;
        }

        /// <summary>
        /// Returns whether a QuoteFile is equal to another object.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {

            //Check for null and compare run-time types.
            if ((obj == null) || !this.GetType().Equals(obj.GetType()))
            {
                return false;
            }
            else
            {
                QuoteFile p = (QuoteFile)obj;

                // Check each collection name to see if the other file has them
                bool matches = true;
                foreach (var item in QuoteCollections)
                {
                    if (!p.ContainsCollection(item.Name))
                    {
                        matches = false;
                        break;
                    }
                }

                return matches;
            }
        }

        public override int GetHashCode()
        {
            // Add hash of each collection name
            int hash = 3049;
            foreach (var collection in QuoteCollections)
            {
                hash = hash + collection.Name.GetHashCode();
            }
            return hash;
        }

        /// <summary>
        /// Thrown when an attempt is made to add a quote to a collection that does not currently belong to this
        /// QuoteFile.
        /// </summary>
        [Serializable]
        public class CollectionNotFoundException : Exception
        {
            public CollectionNotFoundException() { }
            public CollectionNotFoundException(string message) : base(message) { }
            public CollectionNotFoundException(string message, Exception inner) : base(message, inner) { }
            protected CollectionNotFoundException(
              System.Runtime.Serialization.SerializationInfo info,
              System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
        }
    }
}
