﻿namespace BordinBot.Util.Quotes
{
    public class Quote
    {
        /// <summary>
        /// Empty constructor. Leaves all values at empty/null. Intended for use by Json Parser.
        /// </summary>
        public Quote() { }

        /// <summary>
        /// Parameter constructor for quote.
        /// </summary>
        /// <param name="inAuthor">Whoever said this quote.</param>
        /// <param name="inText">The quote spoken by the author.</param>
        /// <param name="inContext">Context/source of quote. Could be context in which it was said, or the book/movie/etc. it came from.</param>
        /// <param name="inDate">Date at which quote was stated.</param>
        public Quote(in string inAuthor, in string inText, in string inContext, in string inDate) => (Author, Text, Context, Date) = (inAuthor, inText, inContext, inDate);

        /// <summary>
        /// Copy contructor.
        /// </summary>
        /// <param name="_quote">Quote to copy.</param>
        public Quote(Quote _quote) : this(_quote.Author, _quote.Text, _quote.Context, _quote.Date) { }

        /// <summary>
        /// Whoever said this quote.
        /// </summary>
        public string Author { get; set; } = "Unknown";

        /// <summary>
        /// The quote spoken by the author.
        /// </summary>
        public string Text { get; set; } = "???";

        /// <summary>
        /// Context/source of quote. Could be context in which it was said, or the book/movie/etc. it came from.
        /// </summary>
        public string Context { get; set; } = "";

        /// <summary>
        /// Date at which quote was stated.
        /// </summary>
        public string Date { get; set; } = "";

        public override string ToString()
        {
            var baseString = string.Format("> \"{0}\"\n> \t- {1}", Text, Author); // Base string with just quote & author

            // Append on optional parts
            if (Context != "")
                baseString += string.Format(", *{0}*", Context); // Italicize
            if (Date != "")
                baseString += string.Format(", {0}", Date);

            return baseString;
        }
    }
}
