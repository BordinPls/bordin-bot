﻿using BordinBot.Util.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace BordinBot.Util.Quotes
{
    class QuoteManager
    {
        /// <summary>
        /// Dictionary of collection name -> (source quote file, collection).
        /// </summary>
        private Dictionary<string, (QuoteFile, QuoteCollection)> mQuotes;

        /// <summary>
        /// All quote files read at startup. List of (File path -> QuoteFile).
        /// </summary>
        private Dictionary<string, QuoteFile> mQuoteFiles;

        /// <summary>
        /// Performs load and save operations on QuoteFiles.
        /// </summary>
        private QuoteFileReaderWriter mQuoteFileReaderWriter = null;

        /// <summary>
        /// Path to folder of quote files relative to this executable.
        /// </summary>
        private string mQuoteFileDirectoryName = "Quotes/";

        /// <summary>
        /// Location and filename for the default quotefile to load (or create, if none found).
        /// </summary>
        private string mDefaultQuoteFilePath = "defaultQuotes.json";

        /// <summary>
        /// Name of collection that new quotes without a named collection will be added to in the default QuoteFile.
        /// </summary>
        private string mDefaultCollectionName = "default";

        private bool mDefaultQuoteFileExists = false;

        /// <summary>
        /// Name of collection that new quotes without a named collection will be added to in the default QuoteFile.
        /// </summary>
        public string DefaultCollectionName
        {
            get { return mDefaultCollectionName; }
            private set { mDefaultCollectionName = value; }
        }

        /// <summary>
        /// QuoteFile that new quotes and collections will automatically go to. Loaded at startup, if found.
        /// </summary>
        private QuoteFile mDefaultQuoteFile;

        /// <summary>
        /// If true, will create a new default quote file in the quotes directory if none is found when the default quote collection is added to.
        /// </summary>
        public bool CreateDefaultQuoteFileIfNotFound { get; }

        // initialize a single Random instance here
        private readonly Random mRandomGen;

        /// <summary>
        /// Constructs a QuoteManager. Attempts to load all QuoteFiles at the location configured on this class.
        /// </summary>

        public QuoteManager()
        {
            mQuoteFiles = new Dictionary<string, QuoteFile>();
            mQuotes = new Dictionary<string, (QuoteFile, QuoteCollection)>();
            mRandomGen = new Random();

            // Create a writer with a path relative to this directory
            string absoluteQuotesDirectory = Path.Combine(Directory.GetCurrentDirectory(), mQuoteFileDirectoryName);
            mQuoteFileReaderWriter = new QuoteFileReaderWriter(absoluteQuotesDirectory, true);

            // Load all quotes
            try
            {
                LoadQuotes();
            }
            catch (Exception e)
            {
                Logger.WriteLine("Failed to load any quote files: {0}", e.Message);
                throw;
            }

            // See if one of the loaded files was the default QuoteFile
            string finalDefaultQuotePath = Path.Combine(absoluteQuotesDirectory, mDefaultQuoteFilePath);
            if (mQuoteFiles.ContainsKey(finalDefaultQuotePath))
            {
                mDefaultQuoteFile = mQuoteFiles[finalDefaultQuotePath];
                mDefaultQuoteFileExists = true;
            }
            // If not, make a new default QuoteFile so it can be added to
            else
            {
                try
                {
                    AddDefaultQuoteFile();
                }
                catch (Exception e)
                {
                    Console.Error.WriteLine("Failed to add default quote file: {0}", e.Message);
                }
            }

        }

        public bool AddQuote(in Quote quote, in string collectionName = "")
        {
            QuoteFile targetQuoteFile = null; // File this collection belongs to

            // If the collection does not already exist, build a new collection with this quote in it and add it
            if (!CollectionExists(collectionName))
            {
                // If default Quotefile ain't loaded, make one
                if (!mDefaultQuoteFileExists)
                {
                    AddDefaultQuoteFile();
                }
                targetQuoteFile = mDefaultQuoteFile;

                // If this is going to the default collection...
                if (collectionName == "")
                {
                    // If the default collection doesn't already exist, make it
                    if (!mQuotes.ContainsKey(mDefaultCollectionName))
                    {
                        AddDefaultCollection();
                    }

                    AddDefaultQuote(quote);
                }
                // Else, it's going to a different one in the default file
                else
                {
                    // If the collection doesn't exist, add it
                    if (!mQuotes.ContainsKey(mDefaultCollectionName))
                    {
                        var targetCollection = new QuoteCollection(collectionName);
                        AddCollection(mDefaultQuoteFile, targetCollection);
                    }

                    mDefaultQuoteFile.AddQuote(quote, collectionName);
                }
            }
            // Else, the collection already exists, and the quote should be added
            else
            {
                // Add the quote
                targetQuoteFile = mQuotes[collectionName].Item1;
                targetQuoteFile.AddQuote(quote, collectionName);
            }

            // Save the collection to file
            SaveQuoteFile(targetQuoteFile);

            return true;
        }

        public void AddCollection(in QuoteFile quoteFile, in QuoteCollection collection)
        {
            // Null param checks
            if (quoteFile == null)
                throw new ArgumentNullException(nameof(quoteFile));
            if (collection == null)
                throw new ArgumentNullException(nameof(collection));

            // Add it to the quotefile, and the list of collections
            quoteFile.AddCollection(collection);
            mQuotes[DefaultCollectionName] = (mDefaultQuoteFile, collection);
        }

        /// <summary>
        /// Gets a random quote from all quote collections. Returns null if no quotes found.
        /// </summary>
        /// <returns>Randomly selected quote, or null if no quotes are loaded.</returns>
        public Quote GetQuote()
        {
            if (mQuotes.Count() > 0)
            {
                var collectionIndex = mRandomGen.Next(mQuotes.Count());
                return mQuotes.ElementAt(collectionIndex).Value.Item2.GetQuote();
            }
            else
                return null;

        }

        /// <summary>
        /// Gets a random quote by an author from all collections. The quote will be randomly selected among 
        /// the first collection to have quotes by that author name. If no quotes can be found among all loaded connections, will
        /// return null.
        /// </summary>
        /// <param name="author">The author.</param>
        /// <returns></returns>
        public Quote GetQuoteBy(in string author)
        {
            Quote outQuote = null;

            // For every collection...
            foreach (var collectionPair in mQuotes)
            {
                var collection = collectionPair.Value.Item2;
                Quote foundQuote = collection.GetQuote(author);

                // If any quote is retrieved for that author, dip
                if (foundQuote != null)
                {
                    outQuote = foundQuote;
                    break;
                }
            }
            return outQuote;
        }

        public Quote GetQuoteFrom(in string collectionName)
        {
            // Null check
            if (collectionName == null) throw new ArgumentNullException("Name of collection was empty.");

            // Check if collection exists
            if (!CollectionExists(collectionName))
            {
                throw new CollectionNotFoundException($"No collection with the name '{collectionName}' was found");
            }

            return mQuotes[collectionName].Item2.GetQuote();
        }

        /// <summary>
        /// Loads all QuoteFiles, clearing the cached quotes on this class if they were already loaded.
        /// </summary>
        public void LoadQuotes()
        {
            // Get all QuoteFiles and their paths
            mQuoteFiles = mQuoteFileReaderWriter.ReadQuoteFiles();

            // Iterate over QuoteFiles to pull out all collections
            foreach (var quoteFilePair in mQuoteFiles)
            {
                foreach (var collection in quoteFilePair.Value.QuoteCollections)
                {
                    try
                    {
                        mQuotes.Add(collection.Name, (quoteFilePair.Value, collection));
                    }
                    // If there's already a collection with the name, try to catch it and note it failed to load!
                    catch (ArgumentException)
                    {
                        var existingQuoteFile = mQuotes[collection.Name].Item1;
                        var existingPath = mQuoteFiles.First(x => x.Value == existingQuoteFile).Key;
                        Console.Error.WriteLine(string.Format("Failed to load collection named {0} from QuoteFile located at {1}; one with the same name already exists {2}"), collection.Name, quoteFilePair.Key, existingPath);
                    }
                }
            }
        }


        private void AddDefaultQuote(in Quote quote)
        {
            // If there's no default quote file, throw
            if (!mDefaultQuoteFileExists)
                throw new InvalidOperationException("default quote file does not exist");

            // Add the default quote collection to the manager if there isn't one
            if (!mQuotes.ContainsKey(DefaultCollectionName))
            {
                throw new InvalidOperationException(String.Format("default collection named {0} not found", mDefaultCollectionName));
            }

            mDefaultQuoteFile.AddQuote(quote, mDefaultCollectionName);
        }

        private void AddDefaultCollection()
        {
            // If there's no default QuoteFile loaded, throw exception
            if (!mDefaultQuoteFileExists)
                throw new InvalidOperationException("cannot add default collection; no default QuoteFile has been loaded");

            var defaultCollection = new QuoteCollection(DefaultCollectionName);
            AddCollection(mDefaultQuoteFile, defaultCollection);
        }

        /// <summary>
        /// Creates a new default QuoteFile and adds it to the list using the default quote file name and path as configured for the manager.
        /// </summary>
        /// <exception cref="InvalidOperationException">Thrown when a default quote file has already been loaded.</exception>
        private void AddDefaultQuoteFile()
        {
            //If the default quote file already exists, throw exception
            if (mDefaultQuoteFileExists)
            {
                throw new InvalidOperationException("attemped to create new DefaultQuoteFile when one already exists");
            }

            // Create quote file
            var defaultFile = new QuoteFile();

            // Attempt to save it
            try
            {
                mQuoteFileReaderWriter.SaveFile(defaultFile, mDefaultQuoteFilePath);
            }
            catch (Exception)
            {
                Logger.WriteLine("Failed to save a new default QuoteFile");
                throw;
            }

            // Attempt to list of QuoteFiles
            mQuoteFiles.Add(mDefaultQuoteFilePath, defaultFile);

            mDefaultQuoteFile = defaultFile;
            mDefaultQuoteFileExists = true;

            Logger.WriteLine("Added new default quote file at path '{0}'", mDefaultQuoteFilePath);
        }

        /// <summary>
        /// Writes the given QuoteFile to the system with a given name. QuoteFile must already be successfully loaded
        /// in order to be saved.
        /// </summary>
        /// <param name="quoteFile">Quotefile to save.</param>
        /// <returns></returns>
        private bool SaveQuoteFile(QuoteFile quoteFile)
        {
            if (quoteFile == null)
                throw new ArgumentNullException("Quotefile parameter was null");

            //Attempt to grab the quoteFile in the list 
            try
            {
                var path = mQuoteFiles.First(x => x.Value == quoteFile).Key;
                mQuoteFileReaderWriter.SaveFile(quoteFile, path);
                return true;

            }
            // Exception means now found; can't save something if we don't have a path
            catch (DirectoryNotFoundException e)
            {
                Console.Error.WriteLine("Failed to save quote file: {1}", e.Message);
            }
            catch (InvalidOperationException)
            {
                throw new Exception("could not save QuoteFile; no path associated with it");
            }

            return false; // Any exceptions hit means failure
        }

        /// <summary>
        /// Returns true if a collection with the collectionName already exists. Case insensitive.
        /// </summary>
        /// <param name="collectionName">Name of collection to search for.</param>
        /// <returns>True if a collection with the given name has been loaded by the manager; false otherwise.</returns>
        public bool CollectionExists(in string collectionName)
        {
            return mQuotes.ContainsKey(collectionName);
        }

        /// <summary>
        /// Returns a list of names, descriptions, and number of quotes for all currently loaded QuoteCollections.
        /// </summary>
        /// <returns>A list of strings formatted "CollectionName: Description (# quotes)". Will be empty if no collections are loaded.</returns>
        public List<string> GetCollectionList()
        {
            var list = new List<string>();

            foreach (var collectionPair in mQuotes.Values)
            {
                list.Add(collectionPair.Item2.ToString());
            }

            return list;
        }

        /// <summary>
        /// Returns whether a quotefile with the given path exists.
        /// </summary>
        /// <param name="path">Path to file with name ending in ".json"</param>
        /// <returns>True if the quotefile with the given path has been loaded.</returns>
        private bool QuoteFileExists(string path)
        {
            // Check null param
            if (path == null)
            {
                throw new ArgumentNullException("FileName was null.");
            }

            // Check param extension
            if (!path.EndsWith(".json"))
            {
                throw new ArgumentException("File name does not end in '.json'");
            }

            try
            {
                var quotefile = mQuoteFiles.Where(file => file.Key == path).Single().Value;

                return quotefile != null; // In case it's somehow a null entry
            }
            // Exception thrown if not found
            catch
            {
                return false;
            }
        }


        /// <summary>
        /// Thrown when a QuoteCollection is created or added but a QuoteCollection of the same name already exists/is loaded.
        /// </summary>
        /// <seealso cref="System.Exception" />
        [Serializable]
        public class DuplicateCollectionException : Exception
        {
            public DuplicateCollectionException() { }
            public DuplicateCollectionException(string message) : base(message) { }
            public DuplicateCollectionException(string message, Exception inner) : base(message, inner) { }
            protected DuplicateCollectionException(
              System.Runtime.Serialization.SerializationInfo info,
              System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
        }

        /// <summary>
        /// Thrown when a QuoteCollection was desired but no QuoteCollection with that name exists on the manager.
        /// </summary>
        [Serializable]
        public class CollectionNotFoundException : Exception
        {
            public CollectionNotFoundException() { }
            public CollectionNotFoundException(string message) : base(message) { }
            public CollectionNotFoundException(string message, Exception inner) : base(message, inner) { }
            protected CollectionNotFoundException(
              System.Runtime.Serialization.SerializationInfo info,
              System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
        }

    }
}
