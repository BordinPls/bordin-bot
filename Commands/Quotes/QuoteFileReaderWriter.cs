﻿using BordinBot.Util.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;

namespace BordinBot.Util.Quotes
{
    /// <summary>
    /// Performs read and write operations for quote files.
    /// </summary>
    public class QuoteFileReaderWriter
    {
        public QuoteFileReaderWriter(in string directoryPath, in bool createDirectoryIfNotFound = true)
        {
            DirectoryPath = directoryPath;
            CreateDirectoryIfNotFound = createDirectoryIfNotFound;
            mFailedDeserializeFiles = new List<string>();
        }

        /// <summary>
        /// Absolute path to use when reading or writing quote files.
        /// </summary>
        public string DirectoryPath { get; }

        /// <summary>
        /// If true, the writer will try to create the directory given by DirectoryPath if not found at the time of saving a QuoteFile.
        /// </summary>
        public bool CreateDirectoryIfNotFound { get; }

        /// <summary>
        /// Maintains a list of file paths that failed to deserialize at runtime. This is to prevent the overwriting of files
        /// that failed to load or deserialize, so they don't get overwritten when an attempt to add a quote is made.
        /// </summary>
        private List<string> mFailedDeserializeFiles;

        /// <summary>
        /// Reads all quote files from the directory path passed into this class, and returns a list of all files read.
        /// </summary>
        /// <returns>A list of (File Path, Quote File); or an default list if none could be read.</returns>
        public Dictionary<string, QuoteFile> ReadQuoteFiles()
        {
            Dictionary<string, QuoteFile> quoteFiles = new Dictionary<string, QuoteFile>();

            // Stop if the path doesn't exist.
            if (!Directory.Exists(DirectoryPath))
            {
                Logger.WriteLine("Path to quote files does not exist ('{0}')", DirectoryPath);
            }
            else
            {
                // Enumerate over all files in the directory
                try
                {
                    var files = Directory.GetFiles(DirectoryPath, "*.json");
                    if (files.Count() == 0)
                    {
                        Logger.WriteLine("No json files were found at {0}!", DirectoryPath);
                        return quoteFiles;
                    }

                    // For every json file found...
                    int numParsed = 0;
                    foreach (var file in files)
                    {
                        try
                        {
                            string jsonstring = File.ReadAllText(file);
                            QuoteFile newQuoteFile = JsonSerializer.Deserialize<QuoteFile>(jsonstring, new JsonSerializerOptions() { PropertyNameCaseInsensitive = true });
                            Logger.WriteLine("Read quote file from '{0}'...", file);
                            quoteFiles.Add(file, newQuoteFile);
                            numParsed++;
                        }
                        catch (JsonException e)
                        {
                            Logger.WriteLine("Failed to read quote file at {0}! {1}", DirectoryPath, e.Message);
                            mFailedDeserializeFiles.Add(Path.Combine(DirectoryPath, file));
                        }
                    }

                    Logger.WriteLine("Read {0} of {1} quote files in total from path '{2}'!", numParsed, files.Count(), DirectoryPath);
                }
                // Catch any unusual errors
                catch (Exception e)
                {
                    Logger.WriteLine("Failed to read quote files: {0}", e.Message);
                    throw;

                }
            }

            return quoteFiles;
        }

        /// <summary>Writes the desired QuoteFile to DirectoryPath using the file name provided.</summary>
        /// <param name="quoteFile">The quote file.</param>
        /// <param name="fileName">Name of the file.</param>
        /// <param name="overwrite">if set to <c>true</c> [overwrite].</param>
        /// <exception cref="System.IO.DirectoryNotFoundException">Thrown when attempting to create the save directory for QuoteFiles but is not allowed to create one as per the config file.</exception>
        /// <exception cref="BordinBot.Util.Quotes.QuoteFileReaderWriter.OverwriteErroredFileException">Thrown when attempting to overwrite a file that failed to load in the first place so that its data isn't unintentionally overwritten.</exception>
        /// <exception cref="BordinBot.Util.Quotes.QuoteFileReaderWriter.OverwriteDisallowedException">Thrown when attempting to overwrite a file but overwriting cannot occur due to the overwrite flag being set to false.</exception>
		public void SaveFile(QuoteFile quoteFile, in string fileName, bool overwrite = true)
        {
            // If the target directory does not exist...
            if (!Directory.Exists(DirectoryPath))
            {
                // And if we're configured to make a new one...
                if (CreateDirectoryIfNotFound)
                {
                    // Attempt to make a new directory
                    Directory.CreateDirectory(DirectoryPath);
                }
                // Else, we don't have a directory and we can't make a new one. Give up and throw an exception.
                else
                {
                    Logger.WriteLine($"directory at {DirectoryPath} could not be found, and a new one is not allowed to be created as per config file");
                    throw new DirectoryNotFoundException(String.Format("directory at {0} could not be found, and a new one is not allowed to be created as per config file", DirectoryPath));
                }
            }

            // Finalize path to new file
            string writePath = Path.Combine(DirectoryPath, fileName);

            //Attempt to serialize the quote file
            JsonSerializerOptions options = new JsonSerializerOptions();
            options.WriteIndented = true;
            string serializedQuoteFile = JsonSerializer.Serialize(quoteFile, options);

            // If the file exists, overwrite it...
            if (File.Exists(writePath))
            {
                // If allowed to overwrite an existing file...
                if (overwrite)
                {
                    // Don't want to overwrite a file that failed to load initially.
                    if (!mFailedDeserializeFiles.Contains(writePath))
                    {
                        Logger.WriteLine("Overwiting file located at {0}...", writePath);
                    }
                    else
                    {
                        Logger.WriteLine($"Cannot overwrite file '{fileName}' that failed to load; may result in unintended data loss", Microsoft.Extensions.Logging.LogLevel.Error);
                        throw new OverwriteErroredFileException($"Failed to save '{fileName}'; this file failed to load, and overwriting it may result in unintended data loss");
                    }
                }
                else
                {
                    Logger.WriteLine("Could not save QuoteFile at '{0}'; file already exists at location, and not allowed to overwrite", Microsoft.Extensions.Logging.LogLevel.Error, writePath);
                    throw new OverwriteDisallowedException($"Could not save QuoteFile at '{writePath}'; file already exists at location, and we're not configured to overwrite QuoteFiles");

                }
            }
            else
            {
                Logger.WriteLine($"Saving new QuoteFile '{fileName}'...");
            }

            File.WriteAllText(writePath, serializedQuoteFile);
        }

        /// <summary>
        /// Indicates an attempt to overwrite a QuoteFile when the overwrite flag is disabled.
        /// </summary>
        [Serializable]
        public class OverwriteDisallowedException : Exception
        {
            public OverwriteDisallowedException() { }
            public OverwriteDisallowedException(string message) : base(message) { }
            public OverwriteDisallowedException(string message, Exception inner) : base(message, inner) { }
            protected OverwriteDisallowedException(
              System.Runtime.Serialization.SerializationInfo info,
              System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
        }

        /// <summary>
        /// Indicates an attempt to overwrite a QuoteFile that failed to properly load in the first place. This is considered an error to order to prevent data loss.
        /// </summary>
        [Serializable]
        public class OverwriteErroredFileException : Exception
        {
            public OverwriteErroredFileException() { }
            public OverwriteErroredFileException(string message) : base(message) { }
            public OverwriteErroredFileException(string message, Exception inner) : base(message, inner) { }
            protected OverwriteErroredFileException(
              System.Runtime.Serialization.SerializationInfo info,
              System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
        }
    }
}
