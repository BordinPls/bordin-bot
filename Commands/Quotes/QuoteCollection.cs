﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace BordinBot.Util.Quotes
{
    /// <summary>
    /// A collection quotes with a common theme
    /// </summary>
    public class QuoteCollection
    {
        /// <summary>
        /// Name of this collection.
        /// </summary>
        [JsonPropertyName("Collection Name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets a description for this collection.
        /// </summary>
        /// <value>
        /// A message that describes the contents of this collection.
        /// </value>
        [JsonPropertyName("Description")]
        public string Description { get; set; }

        /// <summary>
        /// List in which all quotes are stored.
        /// </summary>
        [JsonPropertyName("Quotes")]
        public List<Quote> Quotes
        {
            get
            {
                return mQuotes;
            }
            set
            {
                mQuotes = value;

                // Regenerate the lookup table
                if (mQuotes != null)
                    mQuotesByAuthor = mQuotes.ToLookup(q => q.Author.ToLower());
            }
        }
        private List<Quote> mQuotes;

        /// <summary>
        /// If true, this quote collection will be read-only, and cannot be added to or removed.
        /// </summary>
        [JsonPropertyName("ReadOnly")]
        public bool ReadOnly { get; set; }

        /// <summary>
        /// A lookup table for all the quotes in this collection, with names (to lower'd) to quotes. Allows for easy access and grouping by author. Regenerated every time a new quote is added.
        /// </summary>
        private ILookup<string, Quote> mQuotesByAuthor;

        /// <summary>
        /// Empty constructor. Does not initialize anything.
        /// </summary>
        public QuoteCollection() { }

        /// <summary>
        /// Initializes a quote collection with a specific name.
        /// </summary>
        /// <param name="name">Name of this collection.</param>
        public QuoteCollection(string name, bool inReadOnly = false)
        {
            Name = name;
            mQuotes = new List<Quote>();
            ReadOnly = inReadOnly;
        }

        /// <summary>
        /// Initializes a quote collection with a number of quotes.
        /// </summary>
        /// <param name="inName">Name for this collection.</param>
        /// <param name="inQuotes">List of quotes to insert into collection. May be empty but not null.</param>
        /// <exception cref="ArgumentNullException">Thrown when inQuotes is a null value.</exception>
        public QuoteCollection(string inName, List<Quote> inQuotes, bool ReadOnly = false)
        {
            if (inQuotes == null)
            {
                throw new ArgumentNullException("Attempted to initialize QuoteCollection with null quote list.");
            }

            Name = inName;
            mQuotes = inQuotes;
            this.ReadOnly = ReadOnly;

            // Regenerate the lookup table
            mQuotesByAuthor = mQuotes.ToLookup(q => q.Author.ToLower());
        }

        /// <summary>
        /// Adds a new quote to the collection.
        /// </summary>
        /// <param name="inQuote">Quote to add.</param>
        /// <exception cref="InvalidOperationException">Thrown when attempting to add to a read-only collection.</exception>
        public void AddQuote(in Quote inQuote)
        {
            if (ReadOnly)
            {
                throw new InvalidOperationException(String.Format("Attempted to add quote to read-only collection '{0}'", Name));
            }

            // TODO: Prevent duplicate quote adding

            // Add to list
            mQuotes.Add(inQuote);

            // Regenerate the lookup table
            mQuotesByAuthor = mQuotes.ToLookup(q => q.Author.ToLower());

        }

        /// <summary>
        /// Gets a random quote from this collection.
        /// </summary>
        /// <exception cref="QuoteCollection.EmptyCollectionException"></exception>
        public Quote GetQuote()
        {
            // If there are no quotes in the collection...
            if (mQuotes.Count == 0)
            {
                throw new EmptyCollectionException("No quotes in collection '{0}'.");
            }

            var randomGen = new Random();
            var index = randomGen.Next(mQuotes.Count());
            return mQuotes[index];
        }

        /// <summary>
        /// Returns a random quote from an author. Returns null if none are found.
        /// </summary>
        /// <param name="author">Author to find a random quote for.</param>
        /// <returns>Quote by the author if that author has any quotes; else, null.</returns>
        /// <exception cref="QuoteCollection.EmptyCollectionException">Thrown when there are no quotes in this collection at all.</exception>
        public Quote GetQuote(string author)
        {
            var authorLowercase = author.ToLower();
            // If there aren't any quotes, throw
            if (mQuotes.Count == 0)
            {
                return null;
            }

            // If there are any quotes by that author in this collection...
            if (mQuotesByAuthor.Contains(authorLowercase))
            {
                var allQuotes = mQuotesByAuthor[authorLowercase]; // All quotes by author
                var randomGen = new Random();
                var index = randomGen.Next(allQuotes.Count());
                return allQuotes.ElementAt(index);
            }
            else
            {
                return null;
            }
        }

        public void RemoveQuote(in Quote quote)
        {
            if (mQuotes.Count == 0)
            {
                throw new EmptyCollectionException("No quotes in collection to remove.");
            }

            // TODO: Regenerate the ILookup object.
        }

        /// <summary>
        /// Returns if true if the collection contains any quotes by a given author.
        /// </summary>
        /// <param name="author">Author to search for.</param>
        /// <returns>True at least one quote by the author was found; false otherwise.</returns>
        public bool ContainsAuthor(in string author)
        {
            return mQuotesByAuthor.Contains(author);
        }

        /// <summary>
        /// Converts to string.
        /// </summary>
        /// <returns>
        /// String formatted "CollectionName: Description (# quotes)"
        /// </returns>
        public override string ToString()
        {
            return ($"`{Name} ({Quotes.Count})`: {Description}");
        }

        /// <summary>
        /// Returns a Json string that describes the collection and its current data.
        /// </summary>
        /// <returns></returns>
        public string ToJsonString()
        {
            return JsonSerializer.Serialize(mQuotes);
        }

        /// <summary>
        /// Returns true if the name of two collections is equal.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {

            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }


            return Name == ((QuoteCollection)obj).Name;
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }

        /*****************************************
         * EXCEPTIONS *
         * **************************************/

        /// <summary>
        /// Thrown when the quote collection had no quotes in it but retrieval was attempted.
        /// </summary>
        [Serializable]
        public class EmptyCollectionException : Exception
        {
            public EmptyCollectionException() { }
            public EmptyCollectionException(string message) : base(message) { }
            public EmptyCollectionException(string message, Exception inner) : base(message, inner) { }
            protected EmptyCollectionException(
              System.Runtime.Serialization.SerializationInfo info,
              System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
        }
    }
}
