﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Channels;
using System.Threading.Tasks;
using BordinBot.Commands.Birthdays;
using BordinBot.Util.Quotes;
using BordinBot.Util.Scheduler;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;

namespace BordinBot.Commands
{
    /// <summary>
    /// Provides commands for saving, loading, and displaying quotes.
    /// </summary>
    class QuoteCommands : BaseCommandModule
    {
        #region Methods
        public QuoteCommands()
        {
            mQuoteManager = new QuoteManager();
            AddScheduledTasks();
        }

        /// <summary>
        /// Schedules a daily quote command.
        /// </summary>
        void AddScheduledTasks()
        {
            Scheduler.Instance.ScheduleTask(Task_DailyQuote, DateTime.Parse("01/01/2020 12:00 PM"), EFrequency.Daily);
        }


        [Command("quote")]
        [Description("Provides a random quote from all loaded quotes.")]
        public async Task Quote(CommandContext context)
        {
            var quote = mQuoteManager.GetQuote();
            if (quote != null)
            {
                await context.Channel.SendMessageAsync(quote.ToString());
            }
            else
            {
                await context.RespondAsync("No quotes are currently loaded!");
            }

        }

        [Command("quoteBy")]
        [Description("Provides a random quote by the given author (case insensitive). Will search all quote collections.")]
        public async Task QuoteBy(CommandContext context,
            string author)
        {
            var quote = mQuoteManager.GetQuoteBy(author);
            if (quote != null)
            {
                await context.Channel.SendMessageAsync(quote.ToString());
            }
            else
            {
                await context.RespondAsync("Could not find any loaded quotes by that author!");
            }

        }

        [Command("quoteBy")]
        [Description("Provides a random quote by the given author (case insensitive) from the given collection (case insensitive).")]
        public async Task QuoteBy(CommandContext context,
            string author,
            string collection)
        {
            var quote = mQuoteManager.GetQuoteBy(author);
            await Task.FromResult(0);
        }

        [Command("saveQuote")]
        [Description("Saves a given quote with an author. Can leave later some later optional parameters blank to forego a date or quote context.")]
        public async Task SaveQuote(CommandContext context,
        [Description("Quote spoken by author.")] string quote,
        [Description("Author of quote.")] string author,
        [Description("Name of quote collection to add this to. Leave as \"\" to add to default collection.")] string collection = "",
        [Description("Date spoken. Use \"now\" or leave blank to save date as current MM/DD/YYYY. Can be \"\" to ignore.")] string date = "",
        [Description("Source location or context of quote. Leave as \"\" to ignore if adding later parameters.")] string quoteContext = ""
        )
        {
            var newQuote = new Quote();

            // Set author
            newQuote.Author = author == "" ? "Unknown" : author;

            // Set Text
            newQuote.Text = quote == "" ? "???" : quote;

            // Set date
            if (date == "now" || date == "")
                newQuote.Date = DateTime.Now.ToString("MM/dd/yyyy");

            // Set context
            newQuote.Context = quoteContext;

            // Attempt to add the quote
            mQuoteManager.AddQuote(newQuote, collection);
            await context.RespondAsync("Saved quote!");


        }

        [Command("saveQuote")]
        [Description("Saves a given quote with an author using their Discord @. Can leave later some later optional parameters blank to forego a date or quote context. Any parameters left as \"\" wil be defaulted.")]
        public async Task SaveQuote(CommandContext context,
        [Description("Quote spoken by author. Defaults to '???'")] string quote,
        [Description("Author of quote.")] DiscordUser author,
        [Description("Name of quote collection to add this to. Defaults to the default collection.")] string collection = "default",
        [Description("Date spoken. Defaults to current MM/DD/YYYY. Can also use 'now'.")] string date = "",
        [Description("Source location or context of quote. Leave as \"\" to ignore if adding later parameters.")] string quoteContext = ""
)
        {
            // Generate a quote from the information
            var newQuote = new Quote(quote, author.ToString(), date, quoteContext);

            // Attempt to add the quote, but note what went wrong if something did
            mQuoteManager.AddQuote(newQuote, collection);
            await context.RespondAsync("Saved quote!");
        }

        [Command("quoteFrom")]
        [Description("Provides a random Quote from the given quote collection.")]
        public async Task QuoteFrom(CommandContext context,
        [Description("Name of quote collection to pull a random quote from. If none provided, will attempt to pull from 'default' collection.")] string collection = "default"
)
        {
            var quote = mQuoteManager.GetQuoteFrom(collection);
            if (quote != null)
            {
                await context.Channel.SendMessageAsync(quote.ToString());
            }
            else
            {
                await context.RespondAsync("No quotes are currently loaded!");
            }
        }

        [Command("quoteCollections")]
        [Description("Returns the names and descriptions of all loaded quote collections.")]
        public async Task QuoteCollections(CommandContext context)
        {
            List<string> quoteCollections = mQuoteManager.GetCollectionList();

            // If there are no quotes, let em know
            if (quoteCollections.Count == 0)
            {
                await context.RespondAsync("No quote collections are currently loaded!");
                return;
            }

            // Build content string with all collections
            StringBuilder builder = new StringBuilder();
            foreach (var line in quoteCollections)
            {
                builder.AppendLine(line);
            }

            // Construct embed message
            DiscordEmbedBuilder embedBuilder = new DiscordEmbedBuilder();
            embedBuilder.Description = builder.ToString();
            embedBuilder.Title = "Collections";


            await context.RespondAsync(embedBuilder.Build());

        }

        private void Task_DailyQuote(DiscordChannel channel)
        {
            DiscordEmbedBuilder embedBuild = new DiscordEmbedBuilder();
            embedBuild.Title = "Quote of the Day";
            embedBuild.Description = mQuoteManager.GetQuoteFrom("default").ToString();
            channel.SendMessageAsync(embedBuild.Build());
        }

        #endregion

        #region Members
        /// <summary>
        /// Responsible for loading quote files and providing quotes when requested. Also will save quotes.
        /// </summary>
        private QuoteManager mQuoteManager;
        #endregion

    }
}
