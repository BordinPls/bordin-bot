﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BordinBot.Util.Logging;

namespace BordinBot.Commands.Birthdays
{
    public class BirthdayManager
    {
        #region Constructor
        public BirthdayManager()
        {
            // Load birthdays on startup
            try
            {
                mBirthdays = mBirthdayFile.LoadFile();
            }
            catch (System.IO.FileNotFoundException)
            {
                Logger.WriteLine("Birthday Commands enabled but no birthday file was found!", Microsoft.Extensions.Logging.LogLevel.Warning);
            }
        }
        #endregion

        #region Properties
        /// <summary>
        /// Dictionry of UserMentionString -> Birthday.
        /// </summary>
        Dictionary<string, Birthday> mBirthdays = new Dictionary<string, Birthday>();

        BirthdayFileReaderWriter mBirthdayFile = new BirthdayFileReaderWriter();
        #endregion

        /// <summary>
        /// Adds a new birthday. Will overwrite an existing birthday for a user with a new date if found.
        /// </summary>
        /// <param name="birthday"></param>
        public void AddBirthday(in Birthday birthday)
        {
            mBirthdays[birthday.UserString] = birthday;
            OnBirthdaysChange();
        }

        public void AddBirthday(in string userString, in string birthDate)
        {
            Birthday birthday = new Birthday(userString, birthDate);
            mBirthdays[birthday.UserString] = birthday;
            OnBirthdaysChange();
        }

        /// <summary>
        /// Removes the birthday for a given user.
        /// </summary>
        /// <param name="userString"></param>
        public void RemoveBirthday(in string userString)
        {
            if (mBirthdays.ContainsKey(userString))
            {
                mBirthdays.Remove(userString);
                OnBirthdaysChange();
            }
            else
                throw new KeyNotFoundException("Could not find a birthday for the given user");
        }

        /// <summary>
        /// Returns a list of the birthdays for this month and next month
        /// </summary>
        /// <returns>List of birthdays. May be empty if none are upcoming</returns>
        public List<Birthday> GetBirthdays_Upcoming()
        {
            List<Birthday> birthdays = new List<Birthday>();
            var thisMonth = DateTime.Today.Month;
            foreach (var birthday in mBirthdays.Values)
            {
                // If it's this month or next month, add it
                if (birthday.Birthdate.Month == thisMonth || birthday.Birthdate.Month == thisMonth + 1)
                {
                    birthdays.Add(birthday);
                }
            }

            return birthdays;
        }

        /// <summary>
        /// Returns a list of all birthdays for today.
        /// </summary>
        /// <returns>List of all birthdays for this day. Will be empty if there are none.</returns>
        public List<Birthday> GetBirthdays_Today()
        {
            var birthdays = new List<Birthday>();
            foreach (var birthday in mBirthdays.Values)
            {
                if (birthday.Birthdate.Day == DateTime.Today.Day && birthday.Birthdate.Month == DateTime.Today.Month)
                    birthdays.Add(birthday);
            }

            return birthdays;
        }

        /// <summary>
        /// Returns a list of the birthdays for the given month.
        /// </summary>
        /// <param name="month">Month to check for birthdays for</param>
        /// <returns>Read-only list of birthdays. May be empty if none are upcoming</returns>
        public List<Birthday> GetBirthdays_ByMonth(int month)
        {
            List<Birthday> birthdays = new List<Birthday>();
            var thisMonth = DateTime.Today.Month;
            foreach (var birthday in mBirthdays.Values)
            {
                // If it's this month or next month, add it
                if (birthday.Birthdate.Month == month)
                {
                    birthdays.Add(birthday);
                }
            }

            return birthdays;
        }

        /// <summary>
        /// Returns a list of all birthdays for the server
        /// </summary>
        /// <returns>Read-only list of birthdays. May be empty if none exist</returns>
        public List<Birthday> GetBirthdays_All()
        {
            return mBirthdays.Values.ToList();
        }


        /// <summary>
        /// Called any time the list of birthdays is altered. Overwrites the birthday file
        /// with current data.
        /// </summary>
        public void OnBirthdaysChange()
        {
            mBirthdayFile.WriteBirthdayFile(mBirthdays);
        }
    }
}
