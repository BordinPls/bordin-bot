﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.Json;
using System.Threading.Tasks;
using BordinBot.Util.Logging;

namespace BordinBot.Commands.Birthdays
{
    public class BirthdayFileReaderWriter
    {
        /// <summary>
        /// The name of the Birthdays file.
        /// </summary>
        private const string mFileName = "Birthdays.json";

        public BirthdayFileReaderWriter()
        {

        }

        /// <summary>
        /// Writes a collection of Birthdays to the desired location.
        /// </summary>
        /// <param name="birthdayList">Dictionary of DiscordUserString->Birthday.</param>
        public void WriteBirthdayFile(Dictionary<string, Birthday> birthdayList)
        {
            if (birthdayList == null)
                throw new ArgumentNullException(nameof(birthdayList));

            // Get and check the target directory
            string targetDirectory = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "Data/");

            // If the target directory does not exist...
            if (!Directory.Exists(targetDirectory))
            {
                // Attempt to make a new directory
                Directory.CreateDirectory(targetDirectory);
            }

            //Get file to write to
            string targetPath = Path.Combine(targetDirectory, mFileName);

            // Create serializer
            var options = new JsonSerializerOptions
            {
                WriteIndented = true
            };

            // Serialize objects
            var jsonString = JsonSerializer.Serialize<object>(birthdayList, options);

            // Write to file
            File.WriteAllText(targetPath, jsonString);
        }

        /// <summary>
        /// Loads the Birthdays.json file and returns all read Birthdays.
        /// </summary>
        /// <returns>Dictionary of DiscordUserString -> Birthday read from file.</returns>
        /// <exception cref="System.IO.FileNotFoundException">birthdays.json file not found</exception>
        public Dictionary<string, Birthday> LoadFile()
        {
            // Get and check if the file exists
            string targetPath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "Data/", mFileName);
            // If the target directory does not exist...
            if (!File.Exists(targetPath))
            {
                throw new FileNotFoundException("Birthdays.json file not found");
            }

            // Read json string from file
            string jsonstring = File.ReadAllText(targetPath);
            JsonSerializerOptions jsonOptions = new JsonSerializerOptions();
            Dictionary<string, Birthday> loadedBirthdays = JsonSerializer.Deserialize<Dictionary<String, Birthday>>(jsonstring, jsonOptions);
            Logger.WriteLine($"Successfully read from {mFileName}: {loadedBirthdays.Count} Birthdays loaded");
            return loadedBirthdays;

        }
    }
}
