﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BordinBot.Commands.Birthdays
{
    public class Birthday
    {
        #region Constructor

        public Birthday() { }

        public Birthday(string userString, DateTime date)
        {
            UserString = userString;
            Birthdate = date;
        }


        public Birthday(string userString, string dateAsString)
        {
            UserString = userString;

            // Attempt to parse date from string
            DateTime parsedDate;
            if (DateTime.TryParse(dateAsString, out parsedDate))
            {
                Birthdate = parsedDate;
            }
            else
            {
                // Throw
                throw new ArgumentException("Date provided not in correct format. Must be MM/DD/YYYY");
            }
        }
        #endregion

        #region Properties
        /// <summary>
        /// Mention string of the user with this birthday.
        /// </summary>
        public string UserString { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime Birthdate { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns the current age of this person today.
        /// </summary>
        /// <returns></returns>
        public int Age()
        {
            DateTime now = DateTime.Today;
            int age = now.Year - Birthdate.Year;
            if (Birthdate > now.AddYears(-age)) age--;
            return age;
        }

        /// <summary>
        /// Returns the age that this user will turn on their next birthday.
        /// </summary>
        /// <returns></returns>
        public int ComingAge()
        {
            DateTime now = DateTime.Today;
            int age = now.Year - Birthdate.Year;
            if (Birthdate < now.AddYears(-age)) age++;
            return age;
        }
        #endregion
    }
}
