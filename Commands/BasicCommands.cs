﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;

namespace BordinBot.Commands
{
    class BasicCommands : BaseCommandModule
    {
        [Command("test")]
        [Description("Sanity test for Braden.")]
        public async Task Ping(CommandContext context)
        {
            await context.Channel.SendMessageAsync("Test message, but with auto-deploy now.").ConfigureAwait(false);
        }

        [Command("hello")]
        [Description(":)")]
        public async Task Hello(CommandContext context)
        {
            DiscordEmoji emoji = GetRandomPogEmoji(context);
            await context.Message.RespondAsync($"Hello, {context.User.Mention}! {emoji.ToString()}");
        }

        private DiscordEmoji GetRandomPogEmoji(CommandContext context)
        {
            var emojis = context.Guild.Emojis.Values;

            List<DiscordEmoji> pogEmojis = new List<DiscordEmoji>();

            foreach (DiscordEmoji emoji in emojis)
            {
                if (emoji.Name.Contains("pog"))
                {
                    pogEmojis.Add(emoji);
                }
            }

            Random random = new Random();
            return pogEmojis[random.Next(0, pogEmojis.Count)];
        }
    }
}
