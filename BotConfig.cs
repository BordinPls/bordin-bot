﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace BordinBot
{
    /// <summary>
    /// Represents configuration data for the bot.
    /// </summary>
    public class BotConfig
    {
        #region Properties
        /// <summary>
        /// Private token for this Discord bot. Loaded from "token" tag in Json config file.
        /// </summary>
        [JsonProperty("token")]
        public string Token { get; private set; }
        /// <summary>
        /// Prefix string/character for all commands, e.g. "?" will configure commands to be prefixed with "?". Loaded from "token" tag in Json config file.
        /// </summary>
        [JsonProperty("prefix")]
        public string Prefix { get; private set; }

        /// <summary>
        /// ID of channel where bot messages will be sent.
        /// </summary>
        [JsonProperty("defaultTextChannelID")]
        public ulong DefaultChannelID { get; private set; }

        /// <summary>
        /// If true, bot will send a message to the default text channel upon turning on.
        /// </summary>
        [JsonProperty("sendAwakeMessage")]
        public bool SendAwakeMessage { get; private set; }
        /// <summary>
        /// Message sent to the default text channel on awake, if the default text channel is set.
        /// </summary>
        [JsonProperty("awakeMessage")]
        public string AwakeMessage { get; private set; }
        #endregion
    }
}
